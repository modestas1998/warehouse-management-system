<?php

namespace App\Services;

class RoleService {

    public function checkRole($role, $role_id)
    {
        if ($role == 'admin' && $role_id != 1) {
            abort(403);
        }

        if ($role == 'user' && $role_id != 2) {
            abort(403);
        }

        if ($role == 'products-manager' && $role_id != 3) {
            abort(403);
        }

        if ($role == 'accountant' && $role_id != 4) {
            abort(403);
        }
    }
}
