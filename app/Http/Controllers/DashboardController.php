<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use App\Models\Team;
use App\Models\Warehouse;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $totalWarehouses = Warehouse::where('team_id', auth()->user()->current_team_id)->count();
        $totalUsersInTeam = auth()->user()->currentTeam->users->count();
        $products = 0;
        $productsWithZeroQuantity = 0;
        foreach(Warehouse::where('team_id', auth()->user()->current_team_id)->get() as $warehouse) {
            $products += $warehouse->products()->count();

            foreach($warehouse->products()->get() as $product) {
                if ($product->quantity == 0) {
                    $productsWithZeroQuantity++;
                }
            }
        }
        $totalOrders = Order::where('team_id', auth()->user()->current_team_id)->count();
        $totalTeams = Team::where('owner_id', auth()->user()->id)->count();

        return view('dashboard', compact([
            'totalWarehouses',
            'totalUsersInTeam',
            'products',
            'productsWithZeroQuantity',
            'totalOrders',
            'totalTeams',
        ]));
    }
}
