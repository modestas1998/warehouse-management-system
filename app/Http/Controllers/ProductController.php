<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductCreateRequest;
use App\Http\Requests\ProductEditRequest;
use App\Models\Attribute;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\TemporaryFeaturedImage;
use Illuminate\Http\Request;
use Storage;

class ProductController extends Controller
{
    public function index()
    {
        return view('products.index');
    }

    public function create()
    {
        $categories = Category::where('team_id', auth()->user()->current_team_id)
            ->where('parent_id', 0)
            ->get();

        $attributes = Attribute::where('team_id', auth()->user()->current_team_id)
            ->get();

        return view('products.create', compact('categories', 'attributes'));
    }

    public function store(ProductCreateRequest $request)
    {
        $temporaryGallery = array();
        $request->validated();
        $temporaryFeaturedImage = TemporaryFeaturedImage::where('folder', $request->featured_image)->first();

        $product = Product::create($request->except('featured_image') + ['warehouse_id' => $request->route('id')]);

        if ($request->images_gallery) {
            foreach ($request->images_gallery as $folder) {
                $temporaryGallery[] = TemporaryFeaturedImage::where('folder', $folder)->first();
            }
        }

        if ($temporaryFeaturedImage) {
            $filename = $temporaryFeaturedImage->filename;

            $product->featured_image = $filename;
            $product->save();

            if (!Storage::exists('/public/featuredImages/' . auth()->user()->current_team_id)) {
                Storage::makeDirectory('public/featuredImages/' . auth()->user()->current_team_id, 0777);
            }

            Storage::move(
                '/public/featuredImages/tmp/' . $temporaryFeaturedImage->folder . '/' . $filename,
                '/public/featuredImages/' . auth()->user()->current_team_id . '/' . $filename
            );

            rmdir(storage_path('app/public/featuredImages/tmp/' . $temporaryFeaturedImage->folder));

            $temporaryFeaturedImage->delete();
        }
        if (!empty($temporaryGallery)) {
            if (!Storage::exists('/public/galleryImages/' . auth()->user()->current_team_id)) {
                Storage::makeDirectory('public/galleryImages/' . auth()->user()->current_team_id, 0777);
            }

            foreach ($temporaryGallery as $temporary) {
                ProductImage::create([
                    'image_name' => $temporary->filename,
                    'product_id' => $product->id,
                ]);
                Storage::move(
                    '/public/galleryImages/tmp/' . $temporary->folder . '/' . $temporary->filename,
                    '/public/galleryImages/' . auth()->user()->current_team_id . '/' . $temporary->filename
                );
                rmdir(storage_path('app/public/galleryImages/tmp/' . $temporary->folder));
                $temporary->delete();
            }
        }

        if ($request->selectedCategories) {
            foreach ($request->selectedCategories as $category) {
                $product->categories()->attach($category);
            }
        }

        if ($request->selectedAttributes) {
            foreach ($request->selectedAttributes as $attribute) {
                $product->attributes()->attach($attribute);
            }
        }

        return redirect()->back()->with('success', 'Product created succesfully');
    }

    public function show($id)
    {
        $product = Product::where('id', $id)
            ->with('attributes')
            ->with('categories')
            ->first();

        $gallery = $product->images()->get();

        return view('products.product', compact('product', 'gallery'));
    }

    public function edit($id)
    {
        $product = Product::find($id);

        $categories = Category::where('team_id', auth()->user()->current_team_id)
            ->where('parent_id', 0)
            ->get();

        $attributes = Attribute::where('team_id', auth()->user()->current_team_id)
            ->get();

        return view('products.edit', compact('product', 'categories', 'attributes'));
    }

    public function update(ProductEditRequest $request, $id)
    {
        $input = $request->validated();

        $product = Product::find($id);
        $product->fill($input)->save();

        $product->categories()->detach();
        $product->attributes()->detach();

        if ($request->selectedCategories) {
            foreach ($request->selectedCategories as $category) {
                $product->categories()->attach($category);
            }
        }

        if ($request->selectedAttributes) {
            foreach ($request->selectedAttributes as $attribute) {
                $product->attributes()->attach($attribute);
            }
        }

        return redirect()->route('product.show', ['id' => $id])->with('success', 'Product updated succesfully');
    }
}
