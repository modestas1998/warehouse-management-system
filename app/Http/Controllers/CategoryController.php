<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::where('team_id', auth()->user()->current_team_id)
            ->get();

        return view('categories.index', compact('categories'));
    }
}
