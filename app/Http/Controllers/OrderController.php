<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::where('team_id', auth()->user()->current_team_id)->count();

        return view('orders.index', compact('orders'));
    }

    public function create()
    {
        return view('orders.create');
    }
}
