<?php

namespace App\Http\Controllers;

use App\Models\TemporaryFeaturedImage;
use Illuminate\Http\Request;
use Storage;

class UploadController extends Controller
{
    public function store(Request $request)
    {
        if ($request->hasFile('featured_image')) {
            $file = $request->file('featured_image');
            $filename = uniqid() . '-' . now()->timestamp . '-' . str_replace(' ', '', $file->getClientOriginalName());
            $folder = uniqid() . '-' . auth()->user()->current_team_id . '-' . now()->timestamp;

            Storage::makeDirectory('public/featuredImages/tmp/' . $folder, 0777);

            $file->storeAs('public/featuredImages/tmp/' . $folder, $filename);

            TemporaryFeaturedImage::create([
                'folder' => $folder,
                'filename' => $filename,
            ]);

            return $folder;
        }

        if ($request->hasFile('images_gallery')) {

            $files = $request->file('images_gallery');

            foreach ($files as $file) {
                $folder = uniqid() . '-' . auth()->user()->current_team_id . '-' . now()->timestamp;
                Storage::makeDirectory('public/galleryImages/tmp/' . $folder, 0777);
                $filename = uniqid() . '-' . now()->timestamp . '-' . str_replace(' ', '', $file->getClientOriginalName());
                $file->storeAs('public/galleryImages/tmp/' . $folder, $filename);

                TemporaryFeaturedImage::create([
                    'folder' => $folder,
                    'filename' => $filename,
                ]);

                return $folder;
            }
        }

        return '';
    }

    public function delete()
    {
        $folder = request()->getContent();

        if (Storage::exists('/public/galleryImages/tmp/' . $folder)) {
            array_map('unlink', glob(storage_path('app/public/galleryImages/tmp/' . $folder . '/*.*')));
            rmdir(storage_path('app/public/galleryImages/tmp/' . $folder));
        } else {
            array_map('unlink', glob(storage_path('app/public/featuredImages/tmp/' . $folder . '/*.*')));
            rmdir(storage_path('app/public/featuredImages/tmp/' . $folder));
        }

        $temporaryImage = TemporaryFeaturedImage::where('folder', $folder);
        $temporaryImage->delete();
    }
}
