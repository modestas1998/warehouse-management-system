<?php

namespace App\Http\Livewire;

use App\Models\Attribute;
use Livewire\Component;
use Livewire\WithPagination;

class AttributesTable extends Component
{
    use WithPagination;

    public $name;
    // public $attributes;
    public $editedAttributeIndex = null;
    public $editedAttributeField = null;
    public $perPage = '10';
    public $search = '';


    protected $rules = [
        'allAttributes.*.name' => ['required'],
    ];

    protected $validationAttributes = [
        'allAttributes.*.name' => ['required'],
    ];

    protected $listeners = [
        'refreshAttributesTable' => '$refresh',
        'delete',
    ];

    public function render()
    {
        $allAttributes = Attribute::query()
            ->where('team_id', auth()->user()->current_team_id)
            ->search($this->search)
            ->paginate($this->perPage);

        // $this->attributes = $allAttributes;

        // return view('livewire.attributes-table');
        return view('livewire.attributes-table', compact('allAttributes'));
    }

    // public function mount()
    // {
    //     $attributes = Attribute::query()
    //         ->where('team_id', auth()->user()->current_team_id)
    //         ->search($this->search)
    //         ->paginate($this->perPage);

    //     $this->attributes = $attributes;
    // }

    // public function updated($key)
    // {
    //     $this->validateOnly($key);
    // }

    public function editAttribute($attributeIndex)
    {
        $this->editedAttributeIndex = $attributeIndex;
    }

    public function editAttributeField($attributeIndex, $fieldName)
    {
        $this->editedAttributeField = $attributeIndex . '.' . $fieldName;
    }

    public function saveAttribute($attributeIndex)
    {
        // $this->validate();

        $attribute = $this->attributes[$attributeIndex] ?? null;
        $attribute = $attribute->toArray();
        if (!is_null($attribute)) {
            optional(Attribute::find($attribute['id']))->update($attribute);
        }
        $this->editedAttributeIndex = null;
        $this->editedAttributeField = null;
    }

    public function storeAttribute()
    {
        $this->validate(['name' => 'required']);

        Attribute::create([
            'name' => $this->name,
            'team_id' => auth()->user()->current_team_id,
        ]);

        $this->dispatchBrowserEvent('swal:modal', [
            'type' => 'success',
            'title' => 'Attribute created successfully',
            'text' => '',
        ]);

        $this->name = '';
    }

    public function deleteConfirm($attributeId)
    {
        $this->dispatchBrowserEvent('swal:confirm', [
            'type' => 'warning',
            'title' => 'Are you sure you want to delete this attribute?',
            'text' => '',
            'id' => $attributeId,
        ]);
    }

    public function delete($id)
    {
        Attribute::where('id', $id)->delete();

        $this->emit('refreshAttributesTable');

        $this->dispatchBrowserEvent('swal:modal', [
            'type' => 'success',
            'title' => 'Attribute deleted successfully!',
            'text' => ''
        ]);
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }
}
