<?php

namespace App\Http\Livewire;

use App\Models\Warehouse;
use Livewire\Component;

class WarehouseCreate extends Component
{
    public $name;
    public $country;
    public $city;
    public $address;

    protected $rules = [
        'name' => 'required|max:100',
        'country' => 'required',
        'city' => 'required',
        'address' => 'required',
    ];

    public function render()
    {
        return view('livewire.warehouse-create');
    }

    public function updated($key)
    {
        $this->validateOnly($key);
    }

    public function storePost()
    {
        $this->validate();

        Warehouse::create([
            'name' => $this->name,
            'country' => $this->country,
            'city' => $this->city,
            'address' => $this->address,
            'team_id' => auth()->user()->current_team_id,
        ]);

        $this->dispatchBrowserEvent('swal:modal', [
            'type' => 'success',
            'title' => 'Warehouse created successfully',
            'text' => '',
        ]);

        $this->name = '';
        $this->country = '';
        $this->city = '';
        $this->address = '';
    }
}
