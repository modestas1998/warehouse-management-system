<?php

namespace App\Http\Livewire;

use App\Models\Product;
use App\Models\Warehouse;
use Livewire\Component;

class ProductsTable extends Component
{
    public $warehouses;
    public $warehouseId;
    public $products;
    public $show = false;

    protected $listeners = [
        'delete',
    ];

    public function mount()
    {
        $this->warehouses = Warehouse::where('team_id', auth()->user()->current_team_id)->get();

        if ($this->warehouseId != '') {
            $this->products = Product::where('warehouse_id', $this->warehouseId)->get();
        } else {
            $this->products = [];
        }
    }

    public function updatedWarehouseId()
    {
        if ($this->warehouseId != '') {
            $this->products = Product::where('warehouse_id', $this->warehouseId)->get();
            $this->show = true;
        } else {
            $this->show = false;
        }
    }

    public function render()
    {
        return view('livewire.products-table');
    }

    public function addNewProduct()
    {
        return redirect()->route('product.create', ['id' => $this->warehouseId]);
    }

    public function deleteConfirm($productId)
    {
        $this->dispatchBrowserEvent('swal:confirm', [
            'type' => 'warning',
            'title' => 'Are you sure you want to delete this category with all products?',
            'text' => '',
            'id' => $productId,
        ]);
    }

    public function delete($id)
    {
        Product::where('id', $id)->delete();

        $this->mount();

        $this->dispatchBrowserEvent('swal:modal', [
            'type' => 'success',
            'title' => 'Product deleted successfully!',
            'text' => ''
        ]);
    }
}
