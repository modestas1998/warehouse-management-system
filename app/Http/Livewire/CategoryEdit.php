<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Category;

class CategoryEdit extends Component
{
    public $categories;
    public $category;
    public $categoryName;
    public $categoryId;
    public $categoryParentId;
    public $selectedCategory;

    protected $rules = [
        'categoryName' => 'required|max:100',
    ];

    public function render()
    {
        return view('livewire.category-edit');
    }

    public function mount()
    {
        $this->categoryName = $this->category->name;
        $this->categoryParentId = $this->category->parent_id;
    }

    public function updatedCategoryId()
    {
        if ($this->categoryId != '') {
            $this->selectedCategoryId = $this->categoryId;
        } else {
            $this->selectedCategoryId = 0;
        }
    }

    public function updated($key)
    {
        $this->validateOnly($key);
    }

    public function saveCategory()
    {
        $this->validate();

        $parentId = $this->categoryParentId ? $this->categoryParentId : 0;
        $category = Category::find($this->category->id);
        $category->name = $this->categoryName;
        $category->parent_id = $parentId;
        $category->save();

        $this->dispatchBrowserEvent('swal:modal', [
            'type' => 'success',
            'title' => 'Category updated successfully!',
            'text' => ''
        ]);

        $this->emitTo('categories-table', '$refresh');
    }
}
