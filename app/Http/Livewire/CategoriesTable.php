<?php

namespace App\Http\Livewire;

use App\Models\Category;
use Livewire\Component;

class CategoriesTable extends Component
{
    public $name;
    public $categoryId;
    public $categories;
    public $selectedCategoryId;
    public int $amount = 15;
    public int $offset = 0;

    public bool $showLoadMoreButton;

    protected $rules = [
        'name' => 'required|max:100',
    ];

    protected $listeners = [
        '$refresh',
        'delete',
    ];

    public function mount()
    {
        $this->loadCategories();
    }

    public function updatedCategoryId()
    {
        if ($this->categoryId != '') {
            $this->selectedCategoryId = $this->categoryId;
        } else {
            $this->selectedCategoryId = 0;
        }
    }

    public function render()
    {
        return view('livewire.categories-table');
    }

    public function updated($key)
    {
        $this->validateOnly($key);
    }

    public function storeCategory()
    {
        $this->validate();

        $parentId = $this->selectedCategoryId ?? 0;

        Category::create([
            'name' => $this->name,
            'parent_id' => $parentId,
            'team_id' => auth()->user()->current_team_id,
        ]);

        $this->dispatchBrowserEvent('swal:modal', [
            'type' => 'success',
            'title' => 'Category created successfully',
            'text' => '',
        ]);

        $this->mount();

        $this->name = '';
        $this->categoryId = '';
    }

    public function loadCategories()
    {
        $categories = Category::query()
            ->where('team_id', auth()->user()->current_team_id)
            ->offset($this->offset)
            ->limit($this->amount)
            ->get();

        $this->categories = isset($this->categories) ? $this->categories->merge($categories) : $categories;

        $this->offset += $this->amount;

        $this->showLoadMoreButton = Category::count() > $this->offset;
    }

    public function deleteConfirm($categoryId)
    {
        $this->dispatchBrowserEvent('swal:confirm', [
            'type' => 'warning',
            'title' => 'Are you sure you want to delete this category with all products?',
            'text' => '',
            'id' => $categoryId,
        ]);
    }

    public function delete($id)
    {
        Category::where('id', $id)->delete();

        $this->emit('$refresh');

        $this->dispatchBrowserEvent('swal:modal', [
            'type' => 'success',
            'title' => 'Category deleted successfully!',
            'text' => ''
        ]);
    }
}
