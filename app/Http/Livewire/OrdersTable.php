<?php

namespace App\Http\Livewire;

use App\Models\Order;
use App\Models\Status;
use Livewire\Component;
use Livewire\WithPagination;

class OrdersTable extends Component
{
    use WithPagination;

    public $statuses;
    public $statusId;
    public $sortBy = 'number';
    public $sortDirection = 'asc';
    public $perPage = '10';
    public $search = '';

    protected $listeners = [
        'delete',
        'refreshOrdersTable' => '$refresh',
    ];

    public function render()
    {
        $orders = Order::query()
            ->where('team_id', auth()->user()->current_team_id)
            ->search($this->search)
            ->orderBy($this->sortBy, $this->sortDirection)
            ->paginate($this->perPage);

        return view('livewire.orders-table', ['orders' => $orders]);
    }

    public function mount()
    {
        $this->statuses = Status::all();
    }

    public function deleteConfirm($orderId)
    {
        $this->dispatchBrowserEvent('swal:confirm', [
            'type' => 'warning',
            'title' => 'Are you sure you want to delete this order?',
            'text' => '',
            'id' => $orderId,
        ]);
    }

    public function delete($id)
    {
        $order = Order::findOrFail($id);
        $order->products()->detach();
        $order->billingInformation()->delete();
        $order->shippingInformation()->delete();
        $order->delete();

        $this->mount();

        $this->dispatchBrowserEvent('swal:modal', [
            'type' => 'success',
            'title' => 'Order deleted successfully!',
            'text' => ''
        ]);
    }

    public function sortBy($field)
    {
        if ($this->sortDirection == 'asc') {
            $this->sortDirection = 'desc';
        } else {
            $this->sortDirection = 'asc';
        }

        return $this->sortBy = $field;
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }
}
