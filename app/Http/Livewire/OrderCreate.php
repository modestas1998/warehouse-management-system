<?php

namespace App\Http\Livewire;

use App\Models\BillingInformation;
use App\Models\Order;
use App\Models\Product;
use App\Models\ShippingInformation;
use App\Models\Status;
use Livewire\Component;
use Storage;
use PDF;

class OrderCreate extends Component
{
    public $currentStep = 1;
    public $name;
    public $surname;
    public $country;
    public $city;
    public $address;
    public $zip;
    public $selectedCompany;
    public $selectedSameAsShipping;
    public $companyName;
    public $companyAddress;
    public $companyCode;
    public $shippingCountry;
    public $shippingCity;
    public $shippingAddress;
    public $shippingZip;
    public $visibleExtraFields = false;
    public $sameAsShipping = false;
    public $successMsg = '';
    public $statuses;
    public $statusId;

    public $order;
    public $orderProducts = [];
    public $allProducts = [];

    public function render()
    {
        $total = 0;

        foreach ($this->orderProducts as $orderProduct) {
            if ($orderProduct['is_saved'] && $orderProduct['product_price'] && $orderProduct['quantity']) {
                $total += $orderProduct['product_price'] * $orderProduct['quantity'];
            }
        }

        return view('livewire.order-create', [
            'subtotal' => $total,
            'total' => $total * 1.21 //taxes 21%
        ]);
    }

    public function mount()
    {
        $this->statuses = Status::all();

        $this->allProducts = Product::all();

        if ($this->order) {
            foreach ($this->order->products as $product) {
                $this->orderProducts[] = [
                    'product_id' => $product->id,
                    'quantity' => $product->pivot->quantity,
                    'is_saved' => true,
                    'product_name' => $product->name,
                    'product_price' => $product->regular_price,
                ];
            }
        }
    }

    public function updatedSelectedCompany()
    {
        if ($this->selectedCompany) {
            $this->visibleExtraFields = true;
        } else {
            $this->visibleExtraFields = false;
        }
    }

    public function updatedSelectedSameAsShipping()
    {
        if ($this->selectedSameAsShipping) {
            $this->sameAsShipping = true;
        } else {
            $this->sameAsShipping = false;
        }
    }

    public function firstStepSubmit()
    {
        if ($this->selectedCompany) {
            $this->validate([
                'name' => 'required',
                'surname' => 'required',
                'country' => 'required',
                'city' => 'required',
                'address' => 'required',
                'zip' => 'required',
                'companyName' => 'required',
                'companyAddress' => 'required',
                'companyCode' => 'required',
            ]);
        } else {
            $this->validate([
                'name' => 'required',
                'surname' => 'required',
                'country' => 'required',
                'city' => 'required',
                'address' => 'required',
                'zip' => 'required',
            ]);
        }

        if ($this->sameAsShipping) {
            $this->currentStep = 3;
            $this->shippingCountry = '';
            $this->shippingCity = '';
            $this->shippingAddress = '';
            $this->shippingZip = '';
        } else {
            $this->currentStep = 2;
        }
    }

    public function secondStepSubmit()
    {
        if (!$this->selectedSameAsShipping) {
            $this->validate([
                'shippingCountry' => 'required',
                'shippingCity' => 'required',
                'shippingAddress' => 'required',
                'shippingZip' => 'required',
            ]);
        }

        $this->currentStep = 3;
    }

    public function submitForm()
    {
        $this->validate([
            'statusId' => 'required',
        ]);

        $orderNumber = Order::where('team_id', auth()->user()->current_team_id)->max('number') + 1;

        $order = Order::create([
            'status_id' => $this->statusId,
            'pdf_invoice' => 'pdf',
            'team_id' => auth()->user()->current_team_id,
            'number' => $orderNumber,
        ]);

        $billingInformation = BillingInformation::create([
            'order_id' => $order->id,
            'name' => $this->name,
            'surname' => $this->surname,
            'company_name' => $this->selectedCompany ? $this->companyName : null,
            'company_address' => $this->selectedCompany ? $this->companyAddress : null,
            'company_code' => $this->selectedCompany ? $this->companyCode : null,
            'country' => $this->country,
            'city' => $this->city,
            'address' => $this->address,
            'zip' => $this->zip,
        ]);

        if ($this->selectedSameAsShipping) {
            $shippingInformation = ShippingInformation::create([
                'order_id' => $order->id,
                'country' => $this->country,
                'city' => $this->city,
                'address' => $this->address,
                'zip' => $this->zip,
            ]);
        } else {
            $shippingInformation = ShippingInformation::create([
                'order_id' => $order->id,
                'country' => $this->shippingCountry,
                'city' => $this->shippingCity,
                'address' => $this->shippingAddress,
                'zip' => $this->shippingZip,
            ]);
        }

        $products = [];

        foreach ($this->orderProducts as $product) {
            $products[$product['product_id']] = ['quantity' => $product['quantity']];

            $productModel = Product::find($product['product_id']);
            $productModel->decrement('quantity', $product['quantity']);
        }

        $order->products()->sync($products);

        $filename = $this->generatePdf($order, $billingInformation, $shippingInformation);

        $order->pdf_invoice = $filename;
        $order->save();

        $this->successMsg = 'Order successfully created.';

        $this->clearForm();

        $this->currentStep = 1;
    }

    public function back($step)
    {
        $this->currentStep = $step;
    }

    public function clearForm()
    {
        $this->name = '';
        $this->surname = '';
        $this->country = '';
        $this->city = '';
        $this->address = '';
        $this->zip = '';
        $this->companyName = '';
        $this->companyAddress = '';
        $this->companyCode = '';
        $this->shippingAddress = '';
        $this->shippingCity = '';
        $this->shippingCountry = '';
        $this->shippingZip = '';
        $this->orderProducts = [];
        $this->selectedCompany = false;
        $this->selectedSameAsShipping = false;
        $this->statusId = false;
        $this->visibleExtraFields = false;
    }

    public function addProduct()
    {
        foreach ($this->orderProducts as $key => $orderProduct) {
            if (!$orderProduct['is_saved']) {
                $this->addError('orderProducts.' . $key, 'This line must be saved before creating a new one.');
                return;
            }
        }

        $this->orderProducts[] = [
            'product_id' => '',
            'quantity' => 1,
            'is_saved' => false,
            'product_name' => '',
            'product_price' => 0
        ];
    }

    public function editProduct($index)
    {
        foreach ($this->orderProducts as $key => $orderProduct) {
            if (!$orderProduct['is_saved']) {
                $this->addError('orderProducts.' . $key, 'This line must be saved before editing another.');
                return;
            }
        }

        $this->orderProducts[$index]['is_saved'] = false;
    }

    public function saveProduct($index)
    {
        $this->resetErrorBag();
        $product = $this->allProducts->find($this->orderProducts[$index]['product_id']);
        $this->orderProducts[$index]['product_name'] = $product->name;
        $this->orderProducts[$index]['product_price'] = $product->regular_price;
        $this->orderProducts[$index]['is_saved'] = true;
    }

    public function removeProduct($index)
    {
        unset($this->orderProducts[$index]);
        $this->orderProducts = array_values($this->orderProducts);
    }

    public function generatePdf($order, $billingInformation, $shippingInformation)
    {
        $data = [
            'order' => $order,
            'billingInformation' => $billingInformation,
            'shippingInformation' => $shippingInformation,
        ];

        $filename = $order->id . '-' . date('ymd') . '.pdf';
        $pdf = PDF::loadView('orders.pdf-invoice', $data);
        Storage::put('public/pdf/' . $filename, $pdf->output());

        return $filename;
    }
}
