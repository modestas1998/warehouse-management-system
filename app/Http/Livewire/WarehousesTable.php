<?php

namespace App\Http\Livewire;

use App\Models\Warehouse;
use Illuminate\Support\Collection;
use Livewire\Component;

class WarehousesTable extends Component
{
    public $editedWarehouseIndex = null;
    public $editedWarehouseField = null;
    public int $amount = 10;
    public int $offset = 0;

    public Collection $warehouses;

    public bool $showLoadMoreButton;

    protected $listeners = [
        'delete',
        'refreshWarehouses' => '$refresh',
    ];

    protected $rules = [
        'warehouses.*.name' => ['required'],
        'warehouses.*.country' => ['required'],
        'warehouses.*.city' => ['required'],
        'warehouses.*.address' => ['required'],
    ];

    protected $validationAttributes = [
        'warehouses.*.name' => 'name',
        'warehouses.*.country' => 'country',
        'warehouses.*.city' => 'city',
        'warehouses.*.address' => 'address',
    ];

    public function render()
    {
        return view('livewire.warehouses-table', [
            'warehouses' => $this->warehouses,
        ]);
    }

    public function mount()
    {
        $this->loadWarehouses();
    }

    public function editWarehouse($warehouseIndex)
    {
        $this->editedWarehouseIndex = $warehouseIndex;
    }

    public function editWarehouseField($warehouseIndex, $fieldName)
    {
        $this->editedWarehouseField = $warehouseIndex . '.' . $fieldName;
    }

    public function saveWarehouse($warehouseIndex)
    {
        $this->validate();

        $warehouse = $this->warehouses[$warehouseIndex] ?? null;
        $warehouse = $warehouse->toArray();
        if (!is_null($warehouse)) {
            optional(Warehouse::find($warehouse['id']))->update($warehouse);
        }
        $this->editedWarehouseIndex = null;
        $this->editedWarehouseField = null;
    }

    public function deleteConfirm($warehouseIndex)
    {
        $warehouse = $this->warehouses[$warehouseIndex];
        $warehouse = $warehouse->toArray();
        $this->dispatchBrowserEvent('swal:confirm', [
            'type' => 'warning',
            'title' => 'Are you sure you want to delete this warehouse with all products?',
            'text' => '',
            'id' => $warehouse['id'],
        ]);
    }

    public function delete($id)
    {
        $warehouse = Warehouse::findOrFail($id);
        $warehouse->products()->delete();
        $warehouse->delete();

        $this->emit('refreshWarehouses');

        $this->dispatchBrowserEvent('swal:modal', [
            'type' => 'success',
            'title' => 'Warehouse deleted successfully!',
            'text' => ''
        ]);
    }

    public function loadWarehouses()
    {
        $warehouses = Warehouse::query()
            ->offset($this->offset)
            ->limit($this->amount)
            ->where('team_id', auth()->user()->current_team_id)
            ->get();

        $this->warehouses = isset($this->warehouses) ? $this->warehouses->merge($warehouses) : $warehouses;

        $this->offset += $this->amount;

        $this->showLoadMoreButton = Warehouse::count() > $this->offset;
    }
}
