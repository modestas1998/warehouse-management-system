<?php

namespace App\Http\Livewire;

use App\Models\AttributeValue;
use Livewire\Component;

class AttributeValues extends Component
{
    public $attribute;
    public $name;
    public $values;

    public $editedValueIndex = null;
    public $editedValueField = null;

    protected $rules = [
        'values.*.name' => ['required|max:100'],
    ];

    protected $validationAttributes = [
        'values.*.name' => 'name',
    ];

    protected $listeners = [
        'refreshValues' => '$refresh',
    ];

    public function mount()
    {
        $this->values = AttributeValue::where('attribute_id', $this->attribute->id)->get();
    }

    public function updated($key)
    {
        $this->validateOnly($key);
    }

    public function render()
    {
        return view('livewire.attribute-values');
    }

    public function editValue($valueIndex)
    {
        $this->editedValueIndex = $valueIndex;
    }

    public function editValueField($valueIndex, $fieldName)
    {
        $this->editedValueField = $valueIndex . '.' . $fieldName;
    }

    public function saveValue($valueIndex)
    {
        $this->validate();

        $value = $this->values[$valueIndex] ?? null;
        $value = $value->toArray();
        if (!is_null($value)) {
            optional(AttributeValue::find($value['id']))->update($value);
        }
        $this->editedValueIndex = null;
        $this->editedValueField = null;
    }

    public function saveValues()
    {
        $values = explode(',', $this->name);

        foreach($values as $value) {
            AttributeValue::create([
                'name' => $value,
                'attribute_id' => $this->attribute->id,
            ]);
        }

        $this->mount();

        $this->dispatchBrowserEvent('swal:modal', [
            'type' => 'success',
            'title' => count($values) > 1 ? 'Values created successfully' : 'Value created successfully',
            'text' => '',
        ]);

        $this->name = '';
    }

    public function deleteValue($id)
    {
        AttributeValue::where('id', $id)->delete();

        $this->mount();

        $this->dispatchBrowserEvent('swal:modal', [
            'type' => 'success',
            'title' => 'Value deleted successfully!',
            'text' => ''
        ]);
    }
}
