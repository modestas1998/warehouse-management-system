<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BillingInformation extends Model
{
    use HasFactory;

    protected $fillable = [
        'order_id',
        'name',
        'surname',
        'company_name',
        'company_address',
        'company_code',
        'country',
        'city',
        'address',
        'zip',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
