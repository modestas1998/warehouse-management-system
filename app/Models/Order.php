<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = ['status_id', 'pdf_invoice', 'team_id', 'number'];

    public function products()
    {
        return $this->belongsToMany(Product::class)->withPivot('quantity');
    }

    public function billingInformation()
    {
        return $this->hasOne(BillingInformation::class);
    }

    public function shippingInformation()
    {
        return $this->hasOne(ShippingInformation::class);
    }

    public function getTotalAttribute()
    {
        $total = 0;

        foreach ($this->products as $product) {
            $total += $product->price * $product->pivot->quantity;
        }

        return $total * 1.21; //taxes 21%
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function scopeSearch($query, $value)
    {
        return $query->where('number', 'like', '%'.$value.'%');
    }
}
