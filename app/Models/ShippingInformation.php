<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShippingInformation extends Model
{
    use HasFactory;

    protected $fillable = [
        'order_id',
        'country',
        'city',
        'address',
        'zip',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
