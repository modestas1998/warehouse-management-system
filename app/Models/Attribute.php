<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'team_id',
    ];

    public function values()
    {
        return $this->hasMany(AttributeValue::class);
    }

    public function scopeSearch($query, $value)
    {
        return $query->where('name', 'like', '%'.$value.'%');
    }
}
