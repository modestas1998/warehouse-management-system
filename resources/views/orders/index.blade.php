<x-app-layout>
    <x-slot name="header">
        {{ __('Orders') }}
        <x-add-new href="{{ route('order.create') }}">
            {{ __('Create new order') }}
        </x-add-new>
    </x-slot>

    @if ($orders > 0)
        <div class="bg-white overflow-hidden shadow-md sm:rounded-lg">
            <div class="p-6 border-b border-gray-200">
                @livewire('orders-table')
            </div>
        </div>
    @else
        <div class="flex justify-center align-center py-32">
            No orders
        </div>
    @endif

</x-app-layout>
