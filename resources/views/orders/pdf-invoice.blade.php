<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PDF invoice</title>

    <style>
        body {
            font-size: 16px;
        }

        table {
            width: 100%;
            border-collapse: collapse;
        }

        table tr {
            vertical-align: top;
        }

        table tr td {
            padding: 0;
        }

        table tr td:last-child {
            text-align: right;
        }

        .bold {
            font-weight: bold;
        }

        .right {
            text-align: right;
        }

        .left {
            text-align: left !important;
        }

        .large {
            font-size: 1.75em;
        }

        .total {
            font-weight: bold;
            color: #fb7578;
        }

        .logo-container {
            margin: 20px 0 70px 0;
        }

        .invoice-info-container {
            font-size: 0.875em;
        }

        .invoice-info-container td {
            padding: 4px 0;
        }

        .client-name {
            font-size: 1.5em;
            vertical-align: top;
        }

        .line-items-container {
            margin: 70px 0;
            font-size: 0.875em;
        }

        .line-items-container th {
            text-align: left;
            color: #999;
            border-bottom: 2px solid #ddd;
            padding: 10px 0 15px 0;
            font-size: 0.75em;
            text-transform: uppercase;
        }

        .line-items-container th:last-child {
            text-align: right;
        }

        .line-items-container td {
            padding: 15px 0;
        }

        .line-items-container tbody tr:first-child td {
            padding-top: 25px;
        }

        .line-items-container.has-bottom-border tbody tr:last-child td {
            padding-bottom: 25px;
            border-bottom: 2px solid #ddd;
        }

        .line-items-container.has-bottom-border {
            margin-bottom: 0;
        }

        .line-items-container th.heading-quantity {
            width: 50px;
        }

        .line-items-container th.heading-price {
            text-align: right;
            width: 100px;
        }

        .line-items-container th.heading-subtotal {
            width: 100px;
        }

        .payment-info {
            width: 38%;
            font-size: 0.75em;
            line-height: 1.5;
        }

        .footer {
            margin-top: 100px;
        }

        .footer-thanks {
            font-size: 1.125em;
        }

        .footer-thanks img {
            display: inline-block;
            position: relative;
            top: 1px;
            width: 16px;
            margin-right: 4px;
        }

        .footer-info {
            float: right;
            margin-top: 5px;
            font-size: 0.75em;
            color: #ccc;
        }

        .footer-info span {
            padding: 0 5px;
            color: black;
        }

        .footer-info span:last-child {
            padding-right: 0;
        }

        .page-container {
            display: none;
        }

    </style>
</head>

<body>
    <div class="logo-container">
        <img style="height: 18px" src="https://app.useanvil.com/img/email-logo-black.png">
    </div>

    <table class="invoice-info-container">
        <tr>
            <td class="client-name">
                Invoice
            </td>
            <td class="client-name">
                Billing information
            </td>
            <td class="client-name left">
                Shipping information
            </td>
        </tr>
        <tr>
            <td>
                <p style="line-height: 1.5;">Invoice Date: <strong>{{ date('F j, Y', strtotime($order->created_at)) }}</strong><br>
                    Invoice No: <strong>{{ sprintf('%05d', $order->number) }}</strong>
                </p>
            </td>
            <td>
                <p style="line-height: 1.5;">{{ $billingInformation->name . ' ' . $billingInformation->surname }}<br>
                    {{ $billingInformation->country }}, {{ $billingInformation->address }}, <br>
                    {{ $billingInformation->city }}, {{ $billingInformation->zip }}
                </p>
                @if ($billingInformation->company_name)
                    <p style="line-height: 1.5;"><strong>Company:</strong><br>
                        {{ $billingInformation->company_name }}<br>
                        {{ $billingInformation->company_address }}<br>
                        {{ $billingInformation->company_code }}
                    </p>
                @endif
            </td>
            <td class="left">
                <p style="line-height: 1.5;">{{ $shippingInformation->country }}, {{ $shippingInformation->address }}, <br>
                    {{ $shippingInformation->city }}, {{ $shippingInformation->zip }}
                </p>
            </td>
        </tr>
    </table>


    <table class="line-items-container">
        <thead>
            <tr>
                <th class="heading-quantity">Qty</th>
                <th class="heading-description">Product name</th>
                <th class="heading-price">Price (tax excluded)</th>
                <th class="heading-subtotal">Subtotal (tax excluded)</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($order->products()->get() as $product)
                <tr>
                    <td>{{ $product->pivot->quantity }}</td>
                    <td>{{ $product->name }}</td>
                    <td class="right">$ {{ number_format($product->regular_price, 2) }}</td>
                    <td class="bold">$ {{ number_format($product->regular_price * $product->pivot->quantity, 2) }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>


    <table class="line-items-container has-bottom-border">
        <thead>
            <tr>
                <th>Total</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="border-bottom: 0;">
                    <table style="width: 20%; float: right;">
                        <tr>
                            <td class="left" style="padding: 5px 0; border-bottom: 0"><strong>Subtotal:</strong></td>
                            <td style="padding: 5px 0; border-bottom: 0">$ {{ number_format($order->getTotalAttribute(), 2) }}</td>
                        </tr>
                        <tr>
                            <td class="left" style="padding: 5px 0; border-bottom: 0"><strong>Tax:</strong></td>
                            <td style="padding: 5px 0; border-bottom: 0">21%</td>
                        </tr>
                        <tr>
                            <td class="left" style="padding: 5px 0; border-bottom: 0"><strong>Total:</strong></td>
                            <td style="padding: 5px 0; border-bottom: 0">$ {{ number_format($order->getTotalAttribute() * 1.21, 2) }}</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>

    <div class="footer">
        <div class="footer-info">
            <span>Created by warehouse management system</span>
        </div>
    </div>
</body>

</html>
