<x-app-layout>
    <style>

    </style>
    <x-slot name="header">
        {{ __('Create new order') }}
    </x-slot>

    <x-errors class="mb-4" :errors="$errors" />

    <div class="bg-white overflow-hidden shadow-md sm:rounded-lg">
        <div class="p-6 border-b border-gray-200">
            @livewire('order-create')
        </div>
    </div>
</x-app-layout>
