<div>
    <div class="flex items-center justify-between mb-5">
        <div class="items-center">
            Per page: &nbsp;
            <select wire:model='perPage'
                class="form-select w-20 appearance-none px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding bg-no-repeat border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none">
                <option>5</option>
                <option>10</option>
                <option>15</option>
                <option>20</option>
                <option>25</option>
            </select>
        </div>
        <div>
            <x-label for="search" :value="__('Search for orders by number')" />
            <x-input type="number" wire:model.debounce.300ms="search" id="search"/>
        </div>
    </div>
    <table class="min-w-full leading-normal">
        <thead>
            <tr>
                <th wire:click="sortBy('number')"
                    class="px-5 py-3 cursor-pointer text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
                    Order number
                    @include('partials._sort-Icon', ['field' => 'name'])
                </th>
                <th wire:click="sortBy('status_id')"
                    class="px-5 py-3 cursor-pointer text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
                    Order Status
                    @include('partials._sort-Icon', ['field' => 'status_id'])
                </th>
                <th
                    class="px-5 py-3 text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach ($orders as $index => $order)
                <tr class="hover:bg-gray-100">
                    <td class="px-5 py-5 text-sm border-b border-gray-200">
                        <strong>#{{ sprintf('%05d', $order->number) }}</strong>
                    </td>
                    <td class="px-5 py-5 text-sm border-b border-gray-200">
                        {{-- <select
                            class="form-select appearance-none block w-full px-3 py-1.5 mb-5 text-base font-normal text-gray-700 bg-white bg-clip-padding bg-no-repeat border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                            wire:model="statusId" wire:key="{{ $order->status_id }}" id="statusId">
                            @foreach ($statuses as $status)
                                <option value="{{ $status->id }}"
                                    {{ $order->status_id === $status->id ? 'selected' : '' }}>{{ $status->name }}
                                </option>
                            @endforeach
                        </select> --}}
                        {{ $order->status->name }}
                    </td>
                    <td class="px-5 py-5 text-sm border-b border-gray-200 text-right">
                        <a href="{{ asset('/storage/pdf/' . $order->pdf_invoice) }}" target="_blank"
                            class="py-2 px-4 text-center bg-indigo-600 rounded-md text-white text-sm hover:bg-indigo-500">
                            <i class="fa-solid fa-file-pdf"></i>
                        </a>
                        <button type="button" wire:click="deleteConfirm({{ $order->id }})"
                            class="py-2 px-4 text-center bg-rose-600 rounded-md text-white text-sm hover:bg-rose-500">
                            <i class="fa-solid fa-trash"></i>
                        </button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="mt-5">
        {{ $orders->links() }}
    </div>
</div>
