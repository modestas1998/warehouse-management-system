<div class="grid grid-cols-1 sm:grid-cols-2 gap-6 mt-4">
    <div class="bg-white overflow-hidden shadow-md sm:rounded-lg">
        <div class="p-6">
            <form wire:submit.prevent="storeAttribute">
                @csrf

                <x-label for="name" :value="__('Attribute name')" />
                <x-input type="text" wire:model="name" id="name" value="{{ old('name') }}" />
                @error('name')
                    <div class="text-sm text-red-500 ml-1">
                        {{ $message }}
                    </div>
                @enderror

                <div class="flex justify-end mt-4">
                    <div class="flex justify-end mt-4">
                        <x-button>
                            {{ __('Create') }}
                        </x-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="bg-white overflow-hidden shadow-md sm:rounded-lg">
        <div class="flex items-end justify-between mb-5 p-6">
            <div class="items-center">
                Per page: &nbsp;
                <select wire:model='perPage'
                    class="form-select w-20 appearance-none px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding bg-no-repeat border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none">
                    <option>5</option>
                    <option>10</option>
                    <option>15</option>
                    <option>20</option>
                    <option>25</option>
                </select>
            </div>
            <div>
                <x-label for="search" :value="__('Search for attributes')" />
                <x-input type="text" wire:model.debounce.300ms="search" id="search"/>
            </div>
        </div>
        <div class="p-6">
            <p class="py-2">All attributes</p>
            <ul class="list-inside">
                @foreach ($allAttributes as $index => $attribute)
                    <li value="{{ $attribute->id }}"
                        class="p-3 mb-2 bg-gray-100 shadow-xl sm:rounded-lg flex justify-between items-center"
                        wire:key='{{ $attribute->id }}'>
                        @if ($editedAttributeIndex === $index || $editedAttributeField === $index . '.name')
                            <input type="text"
                                @click.away="$wire.editedAttributeField === '{{ $index }}.name' ? $wire.saveAttribute({{ $index }}) : null"
                                wire:model.defer="attributes.{{ $index }}.name"
                                class="mt-2 text-sm sm:text-base pl-2 pr-4 rounded-lg border w-2/3 py-2 focus:outline-none focus:border-blue-400 {{ $errors->has('attributes.' . $index . '.name') ? 'border-red-500' : 'border-gray-400' }}"/>
                            @if ($errors->has('attributes.' . $index . '.name'))
                                <div class="text-red-500">{{ $errors->first('attributes.' . $index . '.name') }}
                                </div>
                            @endif
                        @else
                            <div class="cursor-pointer" wire:click="editAttributeField({{ $index }}, 'name')">
                                {{ $attribute['name'] }}
                            </div>
                        @endif
                        <div class="">
                            @if ($editedAttributeIndex === $index || (isset($editedAttributeField) && (int) explode('.', $editedAttributeField)[0] === $index))
                                <button
                                    class="py-2 px-4 text-center bg-green-600 rounded-md text-white text-sm hover:bg-green-500"
                                    wire:click.prevent="saveAttribute({{ $index }})">
                                    <i class="fa-solid fa-check"></i>
                                </button>
                            @else
                                <button
                                    class="py-2 px-4 text-center bg-gray-600 rounded-md text-white text-sm hover:bg-gray-500"
                                    wire:click.prevent="editAttribute({{ $index }})">
                                    <i class="fa-solid fa-pencil"></i>
                                </button>
                            @endif
                            <x-modal>
                                <x-slot name="id">
                                    {{ $attribute->id }}
                                </x-slot>
                                <x-slot name="button">
                                    <i class="fa-solid fa-eye"></i>
                                </x-slot>
                                <x-slot name="modalHeader">
                                    Edit attribute {{ $attribute->name }}
                                </x-slot>
                                <x-slot name="modalBody">
                                    @livewire('attribute-values', ['attribute' => $attribute], key($attribute->id))
                                </x-slot>
                            </x-modal>
                            <button type="button" wire:click="deleteConfirm({{ $attribute->id }})"
                                class="py-2 px-4 text-center bg-rose-600 rounded-md text-white text-sm hover:bg-rose-500">
                                <i class="fa-solid fa-trash"></i>
                            </button>
                        </div>
                    </li>
                @endforeach
            </ul>
            <div class="mt-5">
                {{ $allAttributes->links() }}
            </div>
        </div>
    </div>
</div>
