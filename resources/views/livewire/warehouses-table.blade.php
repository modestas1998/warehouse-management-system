<div>
    <table class="min-w-full leading-normal">
        <thead>
            <tr>
                <th
                    class="px-5 py-3 text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
                    Name
                </th>
                <th
                    class="px-5 py-3 text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
                    Country
                </th>
                <th
                    class="px-5 py-3 text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
                    City
                </th>
                <th
                    class="px-5 py-3 text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
                    Address
                </th>
                <th
                    class="px-5 py-3 text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach ($warehouses->toArray() as $index => $warehouse)
                <tr class="hover:bg-gray-100">
                    <td class="px-5 py-5 text-sm border-b border-gray-200">
                        @if ($editedWarehouseIndex === $index || $editedWarehouseField === $index . '.name')
                            <input type="text"
                                @click.away="$wire.editedWarehouseField === '{{ $index }}.name' ? $wire.saveWarehouse({{ $index }}) : null"
                                wire:model.defer="warehouses.{{ $index }}.name"
                                class="mt-2 text-sm sm:text-base pl-2 pr-4 rounded-lg border w-full py-2 focus:outline-none focus:border-blue-400 {{ $errors->has('warehouses.' . $index . '.name') ? 'border-red-500' : 'border-gray-400' }}" />
                            @if ($errors->has('warehouses.' . $index . '.name'))
                                <div class="text-red-500">{{ $errors->first('warehouses.' . $index . '.name') }}
                                </div>
                            @endif
                        @else
                            <div class="cursor-pointer" wire:click="editWarehouseField({{ $index }}, 'name')">
                                {{ $warehouse['name'] }}
                            </div>
                        @endif
                    </td>
                    <td class="px-5 py-5 text-sm border-b border-gray-200">
                        @if ($editedWarehouseIndex === $index || $editedWarehouseField === $index . '.country')
                            <input type="text"
                                @click.away="$wire.editedWarehouseField === '{{ $index }}.country' ? $wire.saveWarehouse({{ $index }}) : null"
                                wire:model.defer="warehouses.{{ $index }}.country"
                                class="mt-2 text-sm sm:text-base pl-2 pr-4 rounded-lg border w-full py-2 focus:outline-none focus:border-blue-400 {{ $errors->has('warehouses.' . $index . '.country') ? 'border-red-500' : 'border-gray-400' }}" />
                            @if ($errors->has('warehouses.' . $index . '.country'))
                                <div class="text-red-500">{{ $errors->first('warehouses.' . $index . '.country') }}
                                </div>
                            @endif
                        @else
                            <div class="cursor-pointer" wire:click="editWarehouseField({{ $index }}, 'country')">
                                {{ $warehouse['country'] }}
                            </div>
                        @endif
                    </td>
                    <td class="px-5 py-5 text-sm border-b border-gray-200">
                        @if ($editedWarehouseIndex === $index || $editedWarehouseField === $index . '.city')
                            <input type="text"
                                @click.away="$wire.editedWarehouseField === '{{ $index }}.city' ? $wire.saveWarehouse({{ $index }}) : null"
                                wire:model.defer="warehouses.{{ $index }}.city"
                                class="mt-2 text-sm sm:text-base pl-2 pr-4 rounded-lg border w-full py-2 focus:outline-none focus:border-blue-400 {{ $errors->has('warehouses.' . $index . '.city') ? 'border-red-500' : 'border-gray-400' }}" />
                            @if ($errors->has('warehouses.' . $index . '.city'))
                                <div class="text-red-500">{{ $errors->first('warehouses.' . $index . '.city') }}
                                </div>
                            @endif
                        @else
                            <div class="cursor-pointer" wire:click="editWarehouseField({{ $index }}, 'city')">
                                {{ $warehouse['city'] }}
                            </div>
                        @endif
                    </td>
                    <td class="px-5 py-5 text-sm border-b border-gray-200">
                        @if ($editedWarehouseIndex === $index || $editedWarehouseField === $index . '.address')
                            <input type="text"
                                @click.away="$wire.editedWarehouseField === '{{ $index }}.address' ? $wire.saveWarehouse({{ $index }}) : null"
                                wire:model.defer="warehouses.{{ $index }}.address"
                                class="mt-2 text-sm sm:text-base pl-2 pr-4 rounded-lg border w-full py-2 focus:outline-none focus:border-blue-400 {{ $errors->has('warehouses.' . $index . '.address') ? 'border-red-500' : 'border-gray-400' }}" />
                            @if ($errors->has('warehouses.' . $index . '.address'))
                                <div class="text-red-500">{{ $errors->first('warehouses.' . $index . '.address') }}
                                </div>
                            @endif
                        @else
                            <div class="cursor-pointer"
                                wire:click="editWarehouseField({{ $index }}, 'address')">
                                {{ $warehouse['address'] }}
                            </div>
                        @endif
                    </td>
                    <td class="px-5 py-5 text-sm border-b border-gray-200 text-right">
                        @if ($editedWarehouseIndex === $index || (isset($editedWarehouseField) && (int) explode('.', $editedWarehouseField)[0] === $index))
                            <button
                                class="py-2 px-4 text-center bg-green-600 rounded-md text-white text-sm hover:bg-green-500"
                                wire:click.prevent="saveWarehouse({{ $index }})">
                                <i class="fa-solid fa-check"></i>
                            </button>
                        @else
                            <button
                                class="py-2 px-4 text-center bg-indigo-600 rounded-md text-white text-sm hover:bg-indigo-500"
                                wire:click.prevent="editWarehouse({{ $index }})">
                                <i class="fa-solid fa-pencil"></i>
                            </button>
                        @endif
                        <button type="button" wire:click="deleteConfirm({{ $index }})" class="py-2 px-4 text-center bg-rose-600 rounded-md text-white text-sm hover:bg-rose-500">
                            <i class="fa-solid fa-trash"></i>
                        </button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    @if ($showLoadMoreButton)
        <button wire:click="loadWarehouses"
                type="button"
                class="inline-flex items-center px-4 py-2 mt-5 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">Load more...</button>
    @endif
</div>
