<div class="grid grid-cols-1 sm:grid-cols-2 gap-6 mt-4">
    <div class="bg-white overflow-hidden shadow-md sm:rounded-lg">
        <div class="p-6">
            <form wire:submit.prevent="storeCategory">
                @csrf

                <x-label for="categoryId" :value="__('Parent category name')" />
                <select
                    class="form-select appearance-none block w-full px-3 py-1.5 mb-5 text-base font-normal text-gray-700 bg-white bg-clip-padding bg-no-repeat border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                    wire:model="categoryId" id="categoryId">
                    <option value="">--</option>
                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                </select>

                <x-label for="name" :value="__('Category name')" />
                <x-input type="text" wire:model="name" id="name" value="{{ old('name') }}" />
                @error('name')
                    <div class="text-sm text-red-500 ml-1">
                        {{ $message }}
                    </div>
                @enderror

                <div class="flex justify-end mt-4">
                    <div class="flex justify-end mt-4">
                        <x-button>
                            {{ __('Create') }}
                        </x-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="bg-white overflow-hidden shadow-md sm:rounded-lg">
        <div class="p-6">
            <p class="py-2">All categories</p>
            <ul class="list-inside">
                @foreach ($categories->where('parent_id', 0) as $category)
                    @include('categories.subcategories', ['category' => $category])
                @endforeach
            </ul>
            @if ($showLoadMoreButton)
                <button wire:click="loadCategories" type="button"
                    class="inline-flex items-center px-4 py-2 mt-5 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                    Load more...
                </button>
            @endif
        </div>
    </div>
</div>
