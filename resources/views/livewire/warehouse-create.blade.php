<form wire:submit.prevent="storePost">
    @csrf

    <div class="grid grid-cols-1 sm:grid-cols-2 gap-6 mt-4">
        <div>
            <x-label for="name" :value="__('Warehouse name')" />
            <x-input type="text" wire:model="name" id="name" value="{{ old('name') }}" />
            @error('name')
                <div class="text-sm text-red-500 ml-1">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <div>
            <x-label for="country" :value="__('Country')" />
            <x-input type="text" wire:model="country" id="country" value="{{ old('country') }}" />
            @error('country')
                <div class="text-sm text-red-500 ml-1">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <div>
            <x-label for="city" :value="__('City')" />
            <x-input type="text" wire:model="city" id="city" />
            @error('city')
                <div class="text-sm text-red-500 ml-1">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <div>
            <x-label for="address" :value="__('Address')" />
            <x-input type="text" wire:model="address" id="address" />
            @error('address')
                <div class="text-sm text-red-500 ml-1">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>

    <div class="flex justify-end mt-4">
        <x-button>
            {{ __('Submit') }}
        </x-button>
    </div>

</form>
