<div>
    <div class="flex justify-between items-center">
        <select
            class="form-select appearance-none block w-2/6 px-3 py-1.5 mb-5 text-base font-normal text-gray-700 bg-white bg-clip-padding bg-no-repeat border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
            wire:model="warehouseId" id="warehouseId">
            <option value="">Select warehouse</option>
            @foreach ($warehouses as $warehouse)
                <option value="{{ $warehouse->id }}">{{ $warehouse->name }}</option>
            @endforeach
        </select>
        @if ($show)
            <x-add-new wire:click="addNewProduct" class="mb-5 cursor-pointer">
                {{ __('Add new product to this warehouse') }}
            </x-add-new>
        @endif
    </div>

    {{-- @foreach ($products as $product)
        {{ $product }}
    @endforeach --}}
    @if ($show)
        @if (!$products->isEmpty())
            <table class="min-w-full leading-normal">
                <thead>
                    <tr>
                        <th
                            class="px-5 py-3 text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
                            Image
                        </th>
                        <th
                            class="px-5 py-3 text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
                            Title
                        </th>
                        <th
                            class="px-5 py-3 text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
                            Quantity
                        </th>
                        <th
                            class="px-5 py-3 text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
                            SKU
                        </th>
                        <th
                            class="px-5 py-3 text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($products as $product)
                        <tr>
                            <td class="px-5 py-5 text-sm bg-white border-b border-gray-200">
                                <img src="{{ asset('storage/featuredImages/' . auth()->user()->current_team_id . '/' . $product->featured_image) }}"
                                    alt="" width="50px">
                            </td>
                            <td class="px-5 py-5 text-sm bg-white border-b border-gray-200">
                                <p class="text-gray-900 whitespace-no-wrap">{{ $product->name }}</p>
                            </td>
                            <td class="px-5 py-5 text-sm bg-white border-b border-gray-200">
                                <p class="text-gray-900 whitespace-no-wrap">{{ $product->quantity }}</p>
                            </td>
                            <td class="px-5 py-5 text-sm bg-white border-b border-gray-200">
                                <p class="text-gray-900 whitespace-no-wrap">{{ $product->sku }}</p>
                            </td>
                            <td class="px-5 py-5 text-sm bg-white border-b border-gray-200">
                                <a href="{{ route('product.show', ['id' => $product->id]) }}"
                                    class="py-2 px-4 text-center bg-green-600 rounded-md text-white text-sm hover:bg-green-500"><i
                                        class="fa-solid fa-eye"></i></a>
                                <button type="button" wire:click="deleteConfirm({{ $product->id }})"
                                    class="py-2 px-4 text-center bg-rose-600 rounded-md text-white text-sm hover:bg-rose-500">
                                    <i class="fa-solid fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <div class="flex justify-center align-center py-32">
                No products in this warehouse
            </div>
        @endif
    @endif
</div>
