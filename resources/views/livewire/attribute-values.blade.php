<div class="grid grid-cols-1 sm:grid-cols-2 gap-6 mt-4">
    <div class="bg-white overflow-hidden shadow-md sm:rounded-lg">
        <div class="p-6">
            <p>{{ __('Attribute name: ') }}{{ $attribute->name }}</p>
            <p class="mt-2">{{ __('Attribute values:') }}</p>
            <ul class="list-inside mt-4 h-96 overflow-y-scroll	">
                @foreach ($values as $index => $value)
                    <li value="{{ $value->id }}"
                        class="p-3 mb-2 bg-gray-100 shadow-xl sm:rounded-lg flex justify-between items-center"
                        wire:key='{{ $value->id }}'>
                        @if ($editedValueIndex === $index || $editedValueField === $index . '.name')
                            <input type="text"
                                @click.away="$wire.editedValueField === '{{ $index }}.name' ? $wire.saveValue({{ $index }}) : null"
                                wire:model.defer="values.{{ $index }}.name"
                                class="mt-2 text-sm sm:text-base pl-2 pr-4 rounded-lg border w-2/3 py-2 focus:outline-none focus:border-blue-400 {{ $errors->has('values.' . $index . '.name') ? 'border-red-500' : 'border-gray-400' }}"/>
                            @if ($errors->has('values.' . $index . '.name'))
                                <div class="text-red-500">{{ $errors->first('values.' . $index . '.name') }}
                                </div>
                            @endif
                        @else
                            <div class="cursor-pointer" wire:click="editValueField({{ $index }}, 'name')">
                                {{ $value['name'] }}
                            </div>
                        @endif
                        <div>
                            @if ($editedValueIndex === $index || (isset($editedValueField) && (int) explode('.', $editedValueField)[0] === $index))
                                <button
                                    class="py-2 px-4 text-center bg-green-600 rounded-md text-white text-sm hover:bg-green-500"
                                    wire:click.prevent="saveValue({{ $index }})">
                                    <i class="fa-solid fa-check"></i>
                                </button>
                            @else
                                <button
                                    class="py-2 px-4 text-center bg-gray-600 rounded-md text-white text-sm hover:bg-gray-500"
                                    wire:click.prevent="editValue({{ $index }})">
                                    <i class="fa-solid fa-pencil"></i>
                                </button>
                            @endif
                            <button type="button" wire:click="deleteValue({{ $value->id }})"
                                class="py-2 px-4 text-center bg-rose-600 rounded-md text-white text-sm hover:bg-rose-500">
                                <i class="fa-solid fa-trash"></i>
                            </button>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
    <div class="bg-white overflow-hidden shadow-md sm:rounded-lg">
        <div class="p-6">
            <form wire:submit.prevent="saveValues">
                @csrf
                <x-label for="name" :value="__('Add values to this attribute (seperated by ,)')" />
                <x-input type="text" wire:model="name" id="name" value="{{ old('name') }}" />
                @error('name')
                    <div class="text-sm text-red-500 ml-1">
                        {{ $message }}
                    </div>
                @enderror

                <div class="flex justify-end mt-4">
                    <div class="flex justify-end mt-4">
                        <x-button>
                            {{ __('Add values') }}
                        </x-button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
