<div>
    <form wire:submit.prevent="saveCategory">
        @csrf
        <x-label for="categoryParentId" :value="__('Parent category name')" />
        <select
            class="form-select appearance-none block w-full px-3 py-1.5 mb-5 text-base font-normal text-gray-700 bg-white bg-clip-padding bg-no-repeat border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
            wire:model="categoryParentId" id="categoryParentId">
            <option value="">--</option>
            @foreach ($categories as $cat)
                <option value="{{ $cat->id }}" {{ ($category->parent_id == $cat->id) ? 'selected' : '' }}>{{ $cat->name }}</option>
            @endforeach
        </select>

        <x-label for="name" :value="__('Category name')" />
        <x-input type="text" wire:model="categoryName" id="name" value="{{ $category->name }}" />
        @error('categoryName')
            <div class="text-sm text-red-500 ml-1">
                {{ $message }}
            </div>
        @enderror

        <div class="flex justify-end mt-4">
            <div class="flex justify-end mt-4">
                <x-button data-modal-toggle="large-modal-{{ $category->id }}" wire:key="{{ $category->id }}">
                    {{ __('Update category') }}
                </x-button>
            </div>
        </div>
    </form>
</div>
