<div>
    {{-- @if (!empty($successMsg))
        <div class="alert alert-success">
            {{ $successMsg }}
        </div>
    @endif --}}
    <div class="stepwizard pt-5">
        <div class="stepwizard-row setup-panel">
            <div class="multi-wizard-step">
                <span class="py-5 px-6 text-center bg-green-500 rounded-full text-white text-sm">
                    1</span>
                <p class="pt-5">Billing<br>information</p>
            </div>
            <div class="multi-wizard-step">
                <span
                    class="{{ $currentStep != 2 && $currentStep == 1? 'py-5 px-6 text-center bg-gray-500 rounded-full text-white text-sm': 'py-5 px-6 text-center bg-green-500 rounded-full text-white text-sm' }}">2</span>
                <p class="pt-5">Shipping<br>information</p>
            </div>
            <div class="multi-wizard-step">
                <span
                    class="{{ $currentStep != 3? 'py-5 px-6 text-center bg-gray-500 rounded-full text-white text-sm': 'py-5 px-6 text-center bg-green-500 rounded-full text-white text-sm' }}"
                    disabled="disabled">3</span>
                <p class="pt-5">Order<br>information</p>
            </div>
        </div>
    </div>

    {{-- first step --}}
    <div class="setup-content {{ $currentStep != 1 ? 'display-none' : '' }}" id="step-1">
        <div class="grid grid-cols-1 sm:grid-cols-2 gap-6 mt-10">
            <div>
                <x-label for="name" :value="__('Name')" />
                <x-input type="text" wire:model="name" id="name" value="{{ old('name') }}" />
                @error('name')
                    <div class="text-sm text-red-500 ml-1">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div>
                <x-label for="surname" :value="__('Surname')" />
                <x-input type="text" wire:model="surname" id="surname" value="{{ old('surname') }}" />
                @error('surname')
                    <div class="text-sm text-red-500 ml-1">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div>
                <x-label for="country" :value="__('Country')" />
                <x-input type="text" wire:model="country" id="country" value="{{ old('country') }}" />
                @error('country')
                    <div class="text-sm text-red-500 ml-1">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div>
                <x-label for="city" :value="__('City')" />
                <x-input type="text" wire:model="city" id="city" />
                @error('city')
                    <div class="text-sm text-red-500 ml-1">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div>
                <x-label for="address" :value="__('Address')" />
                <x-input type="text" wire:model="address" id="address" />
                @error('address')
                    <div class="text-sm text-red-500 ml-1">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div>
                <x-label for="zip" :value="__('Zip code')" />
                <x-input type="text" wire:model="zip" id="zip" />
                @error('zip')
                    <div class="text-sm text-red-500 ml-1">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="col-span-2">
                <label class="inline-flex items-center">
                    <input type="checkbox" class="form-checkbox text-indigo-600" name="selectedCompany"
                        wire:model='selectedCompany'>
                    <span class="mx-2 text-gray-600 text-sm">Order for company?</span>
                </label>
            </div>
            @if ($visibleExtraFields)
                <div>
                    <x-label for="companyName" :value="__('Company name')" />
                    <x-input type="text" wire:model="companyName" id="companyName" />
                    @error('companyName')
                        <div class="text-sm text-red-500 ml-1">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div>
                    <x-label for="companyAddress" :value="__('Company address')" />
                    <x-input type="text" wire:model="companyAddress" id="companyAddress" />
                    @error('companyAddress')
                        <div class="text-sm text-red-500 ml-1">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div>
                    <x-label for="companyCode" :value="__('Company code')" />
                    <x-input type="text" wire:model="companyCode" id="companyCode" />
                    @error('companyCode')
                        <div class="text-sm text-red-500 ml-1">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            @endif
            <div class="col-span-2">
                <label class="inline-flex items-center">
                    <input type="checkbox" class="form-checkbox text-indigo-600" name="selectedSameAsShipping"
                        wire:model='selectedSameAsShipping'>
                    <span class="mx-2 text-gray-600 text-sm">Same as shipping?</span>
                </label>
            </div>
        </div>

        <div class="flex justify-end mt-4">
            <button class="py-2 px-4 rounded-md bg-blue-500 text-center text-white" type="button"
                wire:click="firstStepSubmit">Next</button>
        </div>
    </div>

    {{-- second step --}}
    <div class="setup-content {{ $currentStep != 2 ? 'display-none' : '' }}" id="step-2">
        <div class="grid grid-cols-1 sm:grid-cols-2 gap-6 mt-10">
            <div>
                <x-label for="shippingCountry" :value="__('Shipping country')" />
                <x-input type="text" wire:model="shippingCountry" id="shippingCountry"
                    value="{{ old('shippingCountry') }}" />
                @error('shippingCountry')
                    <div class="text-sm text-red-500 ml-1">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div>
                <x-label for="shippingCity" :value="__('Shipping city')" />
                <x-input type="text" wire:model="shippingCity" id="shippingCity" />
                @error('shippingCity')
                    <div class="text-sm text-red-500 ml-1">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div>
                <x-label for="shippingAddress" :value="__('Shipping address')" />
                <x-input type="text" wire:model="shippingAddress" id="shippingAddress" />
                @error('shippingAddress')
                    <div class="text-sm text-red-500 ml-1">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div>
                <x-label for="shippingZip" :value="__('Shipping zip code')" />
                <x-input type="text" wire:model="shippingZip" id="shippingZip" />
                @error('shippingZip')
                    <div class="text-sm text-red-500 ml-1">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="col-span-2 flex justify-between mt-4">
                <button class="py-2 px-4 rounded-md bg-gray-500 text-center text-white" type="button"
                    wire:click="back(1)">Back</button>
                <button class="py-2 px-4 rounded-md bg-blue-500 text-center text-white" type="button"
                    wire:click="secondStepSubmit">Next</button>
            </div>
        </div>
    </div>
    {{-- third step --}}
    <div class="py-10 setup-content {{ $currentStep != 3 ? 'display-none' : '' }}" id="step-3">
        <select
            class="form-select appearance-none block w-full px-3 py-1.5 mb-5 text-base font-normal text-gray-700 bg-white bg-clip-padding bg-no-repeat border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
            wire:model="statusId" id="statusId">
            <option value="">Select order status</option>
            @foreach ($statuses as $status)
                <option value="{{ $status->id }}">{{ $status->name }}</option>
            @endforeach
        </select>
        @error('statusId')
            <div class="text-sm text-red-500 ml-1 mb-2">
                {{ $message }}
            </div>
        @enderror
        <div class="p-5 border border-gray-200">
            <table class="table table-auto w-full" id="products_table">
                <thead class="bg-gray-400 text-white">
                    <tr>
                        <th class="text-left p-4">Product</th>
                        <th class="text-left p-4" width="150">Quantity</th>
                        <th class="text-left p-4" width="150"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($orderProducts as $index => $orderProduct)
                        <tr>
                            <td class="p-5">
                                @if ($orderProduct['is_saved'])
                                    <input type="hidden" name="orderProducts[{{ $index }}][product_id]"
                                        wire:model="orderProducts.{{ $index }}.product_id" />
                                    @if ($orderProduct['product_name'] && $orderProduct['product_price'])
                                        {{ $orderProduct['product_name'] }}
                                        (${{ number_format($orderProduct['product_price'], 2) }})
                                    @endif
                                @else
                                    <select name="orderProducts[{{ $index }}][product_id]"
                                        class="focus:outline-none w-full border {{ $errors->has('orderProducts.' . $index) ? 'border-red-500' : 'border-indigo-500' }} rounded-md p-1"
                                        wire:model="orderProducts.{{ $index }}.product_id">
                                        <option value="">-- choose product --</option>
                                        @foreach ($allProducts as $product)
                                            <option value="{{ $product->id }}">
                                                {{ $product->name }}
                                                (${{ number_format($product->regular_price, 2) }})
                                            </option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('orderProducts.' . $index))
                                        <em class="text-sm text-red-500">
                                            {{ $errors->first('orderProducts.' . $index) }}
                                        </em>
                                    @endif
                                @endif
                            </td>
                            <td class="p-5">
                                @if ($orderProduct['is_saved'])
                                    <input type="hidden" name="orderProducts[{{ $index }}][quantity]"
                                        wire:model="orderProducts.{{ $index }}.quantity" />
                                    {{ $orderProduct['quantity'] }}
                                @else
                                    <input type="number" name="orderProducts[{{ $index }}][quantity]"
                                        class="focus:outline-none w-full border border-indigo-500 rounded-md p-1"
                                        wire:model="orderProducts.{{ $index }}.quantity" />
                                @endif
                            </td>
                            <td class="p-1">
                                @if ($orderProduct['is_saved'])
                                    <button
                                        class="hover:bg-blue-600 p-1 bg-blue-500 border border-blue-600 rounded-md text-white focus:outline-none"
                                        wire:click.prevent="editProduct({{ $index }})">Edit</button>
                                @elseif($orderProduct['product_id'])
                                    <button
                                        class="hover:bg-green-600 p-1 bg-green-500 border border-green-600 rounded-md text-white focus:outline-none"
                                        wire:click.prevent="saveProduct({{ $index }})">Save</button>
                                @endif
                                <button
                                    class="hover:bg-red-600 ml-1 p-1 bg-red-500 border border-red-600 rounded-md text-white focus:outline-none"
                                    wire:click.prevent="removeProduct({{ $index }})">Delete</button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            <div class="mt-3 ml-5">
                <button
                    class="hover:bg-indigo-600 py-1 px-4 bg-indigo-500 border border-indigo-600 rounded-md text-white focus:outline-none"
                    wire:click.prevent="addProduct">+ Add Product</button>
            </div>

            <div class="flex justify-end">
                <table>
                    <tr class="">
                        <th class="p-2 text-left">Subtotal</th>
                        <td class="p-2 text-right">$ {{ number_format($subtotal, 2) }}</td>
                    </tr>
                    <tr class="border-t border-gray-300">
                        <th class="p-2 text-left">Taxes</th>
                        <td class="p-2 text-right" width="125">21%</td>
                    </tr>
                    <tr class="border-t border-gray-300">
                        <th class="p-2 text-left">Total</th>
                        <td class="p-2 text-right">$ {{ number_format($total, 2) }}</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-span-2 flex justify-between mt-4">
            @if ($sameAsShipping)
                <button class="py-2 px-4 rounded-md bg-gray-500 text-center text-white" type="button"
                    wire:click="back(1)">Back</button>
            @else
                <button class="py-2 px-4 rounded-md bg-gray-500 text-center text-white" type="button"
                    wire:click="back(2)">Back</button>
            @endif
            <button class="py-2 px-4 rounded-md bg-blue-500 text-center text-white" type="button"
                wire:click="submitForm">Create order</button>
        </div>
    </div>
