<form wire:submit.prevent="storeProduct">
    @csrf

    <div class="grid grid-cols-1 gap-10 mt-4 px-32">
        <div>
            <x-label for="name" :value="__('Product name')" />
            <x-input type="text" id="name" value="{{ old('name') }}" />
            {{-- @error('name')
                <div class="text-sm text-red-500 ml-1">
                    {{ $message }}
                </div>
            @enderror --}}
        </div>

        <div>
            <x-label for="short_description" :value="__('Short description')" />
            <x-text-area name="short_description" id="short_description" rows="4">
                {{ old('short_description') }}</x-text-area>
            {{-- @error('short_description')
                <div class="text-sm text-red-500 ml-1">
                    {{ $message }}
                </div>
            @enderror --}}
        </div>

        <div>
            <x-label for="description" :value="__('Description')" />
            <x-text-area name="description" id="description" rows="4">
                {{ old('description') }}</x-text-area>
            {{-- @error('description')
                <div class="text-sm text-red-500 ml-1">
                    {{ $message }}
                </div>
            @enderror --}}
        </div>

        <div class="flex">
            <div class="w-1/2 mr-5">
                <x-label for="regular_price" :value="__('Regular price')" />
                <x-input type="number" id="regular_price" step=".01" />
                {{-- @error('regular_price')
                    <div class="text-sm text-red-500 ml-1">
                        {{ $message }}
                    </div>
                @enderror --}}
            </div>
            <div class="w-1/2">
                <x-label for="sale_price" :value="__('Sale price')" />
                <x-input type="number" id="sale_price" step=".01" />
                {{-- @error('sale_price')
                    <div class="text-sm text-red-500 ml-1">
                        {{ $message }}
                    </div>
                @enderror --}}
            </div>
        </div>

        <div class="flex">
            <div class="w-1/2 mr-5">
                <x-label for="sku" :value="__('SKU')" />
                <x-input type="text" step=".01" />
                {{-- @error('sku')
                    <div class="text-sm text-red-500 ml-1">
                        {{ $message }}
                    </div>
                @enderror --}}
            </div>
            <div class="w-1/2">
                <x-label for="quantity" :value="__('Quantity')" />
                <x-input type="number" id="quantity" />
                {{-- @error('quantity')
                    <div class="text-sm text-red-500 ml-1">
                        {{ $message }}
                    </div>
                @enderror --}}
            </div>
        </div>

        <div class="flex justify-between">
            <div class="w-1/2 mr-5">
                <x-label for="categories" :value="__('Categories')" />
                <div class="h-44 overflow-y-scroll bg-white border border-solid rounded p-5">
                    <ul class="list-inside">
                        @foreach ($categories as $category)
                            @include('products.subcategories', ['category' => $category])
                        @endforeach
                    </ul>
                </div>
            </div>

            <div class="w-1/2">
                <x-label for="attributes" :value="__('Attributes')" />
                <div class="h-44 overflow-y-scroll bg-white border border-solid rounded p-5">
                    <ul class="list-inside">
                        @foreach ($attributes as $attribute)
                            <div>
                                <label class="inline-flex items-center">
                                    <input type="checkbox" class="form-checkbox text-indigo-600"
                                        name="selectedAttributes" wire:model='selectedAttributes'
                                        value="{{ $attribute->id }}">
                                    <span class="mx-2 text-gray-600 text-sm">{{ $attribute->name }}</span>
                                </label>
                            </div>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>

        <div>
            <x-label for="featuredImage" :value="__('Featured image')"/>

            <input type="file" name="featuredImage" wire:model='featuredImage' class="mt-2" id="featuredImage">
        </div>

    </div>

    <div class="flex justify-end mt-4">
        <x-button>
            {{ __('Submit') }}
        </x-button>
    </div>

</form>
