<li value="{{ $category->id }}" class="p-3 mb-2 bg-gray-100 shadow-xl sm:rounded-lg flex justify-between items-center">
    <p>{{ $category->name }}</p>
    <div class="">
        <x-modal>
            <x-slot name="id">
                {{ $category->id }}
            </x-slot>
            <x-slot name="button">
                <i class="fa-solid fa-pencil"></i>
            </x-slot>
            <x-slot name="modalHeader">
                Edit category {{ $category->name }}
            </x-slot>
            <x-slot name="modalBody">
                @livewire('category-edit', ['category' => $category, 'categories' => $categories], key($category->id))
            </x-slot>
        </x-modal>
        <button type="button" wire:click="deleteConfirm({{ $category->id }})" class="py-2 px-4 text-center bg-rose-600 rounded-md text-white text-sm hover:bg-rose-500">
            <i class="fa-solid fa-trash"></i>
        </button>
    </div>
</li>

@if (count($category->categoryChildrens) > 0)
    <ul class="ml-8">
        @foreach ($category->categoryChildrens as $sub)
            @include('categories.subcategories', ['category' => $sub])
        @endforeach
    </ul>
@endif
