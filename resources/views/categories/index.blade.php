<x-app-layout>
    <x-slot name="header">
        {{ __('Categories') }}
    </x-slot>

    <div class="overflow-hidden">
        @livewire('categories-table')
    </div>

</x-app-layout>
