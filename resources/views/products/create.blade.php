<x-app-layout>
    <x-slot name="header">
        {{ __('Create new product') }}
    </x-slot>

    <x-errors class="mb-4" :errors="$errors" />
    @if (Session::has('success'))
        <div id="alert-additional-content-3" class="p-4 mb-4 bg-green-100 rounded-lg dark:bg-green-200" role="alert">
            <div class="flex items-center">
                <svg class="mr-2 w-5 h-5 text-green-700 dark:text-green-800" fill="currentColor" viewBox="0 0 20 20"
                    xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd"
                        d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z"
                        clip-rule="evenodd"></path>
                </svg>
                <h3 class="text-lg font-medium text-green-700 dark:text-green-800">Success</h3>
            </div>
            <div class="mt-2 mb-4 text-sm text-green-700 dark:text-green-800">
                {{ Session::get('success') }}
            </div>
        </div>
    @endif

    <div class="bg-white overflow-hidden shadow-md sm:rounded-lg mb-20">
        <div class="p-6 border-b border-gray-200">
            <form method="POST" action="{{ route('product.store', ['id' => request()->route('id')]) }}"
                enctype="multipart/form-data">
                @csrf

                <div class="grid grid-cols-1 gap-10 mt-4 px-32">
                    <div>
                        <x-label for="name" :value="__('Product name')" />
                        <x-input type="text" name="name" id="name" value="{{ old('name') }}" />
                    </div>

                    <div class="flex">
                        <div class="w-1/2 mr-5">
                            <x-label for="short_description" :value="__('Short description')" />
                            <x-text-area name="short_description" id="short_description" rows="4">
                                {{ old('short_description') }}</x-text-area>
                        </div>
                        <div class="w-1/2">
                            <x-label for="description" :value="__('Description')" />
                            <x-text-area name="description" id="description" rows="4">
                                {{ old('description') }}</x-text-area>
                        </div>
                    </div>

                    <div class="flex">
                        <div class="w-1/2 mr-5">
                            <x-label for="regular_price" :value="__('Regular price')" />
                            <x-input type="number" name="regular_price" id="regular_price" step=".01" />
                        </div>
                        <div class="w-1/2">
                            <x-label for="sale_price" :value="__('Sale price')" />
                            <x-input type="number" name="sale_price" id="sale_price" step=".01" />
                        </div>
                    </div>

                    <div class="flex">
                        <div class="w-1/2 mr-5">
                            <x-label for="sku" :value="__('SKU')" />
                            <x-input type="text" name="sku" step=".01" />
                        </div>
                        <div class="w-1/2">
                            <x-label for="quantity" :value="__('Quantity')" />
                            <x-input type="number" name="quantity" id="quantity" />
                        </div>
                    </div>

                    <div class="flex justify-between">
                        <div class="w-1/2 mr-5">
                            <x-label for="categories" :value="__('Categories')" />
                            <div class="h-44 overflow-y-scroll bg-white border border-solid rounded p-5">
                                <ul class="list-inside">
                                    @foreach ($categories as $category)
                                        @include('products.subcategories', [
                                            'category' => $category,
                                        ])
                                    @endforeach
                                </ul>
                            </div>
                        </div>

                        <div class="w-1/2">
                            <x-label for="attributes" :value="__('Attributes')" />
                            <div class="h-44 overflow-y-scroll bg-white border border-solid rounded p-5">
                                <ul class="list-inside">
                                    @foreach ($attributes as $attribute)
                                        <div>
                                            <label class="inline-flex items-center">
                                                <input type="checkbox" class="form-checkbox text-indigo-600"
                                                    name="selectedAttributes[]" value="{{ $attribute->id }}">
                                                <span class="mx-2 text-gray-600 text-sm">{{ $attribute->name }}</span>
                                            </label>
                                        </div>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div>
                        <x-label for="featuredImage" :value="__('Featured image')" />

                        <input type="file" name="featured_image" wire:model='featuredImage' class="mt-2"
                            id="featuredImage">
                    </div>

                    <div>
                        <x-label for="images_gallery" :value="__('Images gallery')" />

                        <input type="file" name="images_gallery[]" class="mt-2 filepond" id="images_gallery" multiple
                            data-allow-reorder="true" data-max-files="15">
                    </div>
                </div>

                <div class="flex justify-end mt-4">
                    <x-button>
                        {{ __('Submit') }}
                    </x-button>
                </div>

            </form>
        </div>
    </div>

    @section('scripts')
        <script>
            FilePond.registerPlugin(FilePondPluginImagePreview);
            const inputElement = document.querySelector('input[id="featuredImage"]');
            const pond = FilePond.create(inputElement);
            FilePond.setOptions({
                server: {
                    url: '/upload',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    revert: {
                        url: '/delete',
                        headers: {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                        }
                    }
                }
            });
            FilePond.create(
                document.querySelector('input[id="images_gallery"]')
            );
            tinymce.init({
                selector: '#short_description'
            });
            tinymce.init({
                selector: '#description'
            });
        </script>
    @endsection
</x-app-layout>
