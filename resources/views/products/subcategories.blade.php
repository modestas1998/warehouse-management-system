<li>
    <div>
        <label class="inline-flex items-center">
            <input type="checkbox" class="form-checkbox text-indigo-600" name="selectedCategories[]"
                value="{{ $category->id }}"
                @isset($product) {{ $product->categories->contains($category->id) ? 'checked' : '' }} @endisset>
            <span class="mx-2 text-gray-600 text-sm">{{ $category->name }}</span>
        </label>
    </div>
</li>

@if (count($category->categoryChildrens) > 0)
    <ul class="ml-8">
        @foreach ($category->categoryChildrens as $sub)
            @include('products.subcategories', ['category' => $sub])
        @endforeach
    </ul>
@endif
