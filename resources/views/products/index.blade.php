<x-app-layout>
    <x-slot name="header">
        {{ __('Products') }}
    </x-slot>

    <div class="bg-white overflow-hidden shadow-md sm:rounded-lg">
        <div class="p-6 border-b border-gray-200">
            @livewire('products-table')
        </div>
    </div>

</x-app-layout>
