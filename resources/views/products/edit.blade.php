<x-app-layout>
    <x-slot name="header">
        Edit product {{ $product->name }}
    </x-slot>

    <x-errors class="mb-4" :errors="$errors" />

    <div class="bg-white overflow-hidden shadow-md sm:rounded-lg mb-20">
        <div class="p-6 border-b border-gray-200">
            <form method="POST" action="{{ route('product.update', ['id' => $product->id]) }}" enctype="multipart/form-data">
                @method('PUT')

                @csrf

                <div class="grid grid-cols-1 gap-10 mt-4 px-32">
                    <div>
                        <x-label for="name" :value="__('Product name')" />
                        <x-input type="text" name="name" id="name" value="{{ $product->name }}" />
                    </div>

                    <div class="flex">
                        <div class="w-1/2 mr-5">
                            <x-label for="short_description" :value="__('Short description')" />
                            <x-text-area name="short_description" id="short_description" rows="4">
                                {{ $product->short_description }}</x-text-area>
                        </div>
                        <div class="w-1/2">
                            <x-label for="description" :value="__('Description')" />
                            <x-text-area name="description" id="description" rows="4">
                                {{ $product->description }}</x-text-area>
                        </div>
                    </div>

                    <div class="flex">
                        <div class="w-1/2 mr-5">
                            <x-label for="regular_price" :value="__('Regular price')" />
                            <x-input type="number" name="regular_price" id="regular_price" step=".01"
                                value="{{ $product->regular_price }}" />
                        </div>
                        <div class="w-1/2">
                            <x-label for="sale_price" :value="__('Sale price')" />
                            <x-input type="number" name="sale_price" id="sale_price" step=".01"
                                value="{{ $product->sale_price }}" />
                        </div>
                    </div>

                    <div class="flex">
                        <div class="w-1/2 mr-5">
                            <x-label for="sku" :value="__('SKU')" />
                            <x-input type="text" name="sku" step=".01" value="{{ $product->sku }}" />
                        </div>
                        <div class="w-1/2">
                            <x-label for="quantity" :value="__('Quantity')" />
                            <x-input type="number" name="quantity" id="quantity" value="{{ $product->quantity }}" />
                        </div>
                    </div>

                    <div class="flex justify-between">
                        <div class="w-1/2 mr-5">
                            <x-label for="categories" :value="__('Categories')" />
                            <div class="h-44 overflow-y-scroll bg-white border border-solid rounded p-5">
                                <ul class="list-inside">
                                    @foreach ($categories as $category)
                                        @include('products.subcategories', [
                                            'category' => $category,
                                        ])
                                    @endforeach
                                </ul>
                            </div>
                        </div>

                        <div class="w-1/2">
                            <x-label for="attributes" :value="__('Attributes')" />
                            <div class="h-44 overflow-y-scroll bg-white border border-solid rounded p-5">
                                <ul class="list-inside">
                                    @foreach ($attributes as $attribute)
                                        <div>
                                            <label class="inline-flex items-center">
                                                <input type="checkbox" class="form-checkbox text-indigo-600"
                                                    name="selectedAttributes[]" value="{{ $attribute->id }}"
                                                    {{ $product->attributes->contains($attribute->id) ? 'checked' : '' }}>
                                                <span
                                                    class="mx-2 text-gray-600 text-sm">{{ $attribute->name }}</span>
                                            </label>
                                        </div>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="flex justify-end mt-4">
                    <x-button>
                        {{ __('Submit') }}
                    </x-button>
                </div>

            </form>
        </div>
    </div>

    @section('scripts')
        <script>
            FilePond.registerPlugin(FilePondPluginImagePreview);
            const inputElement = document.querySelector('input[id="featuredImage"]');
            const pond = FilePond.create(inputElement);
            FilePond.setOptions({
                server: {
                    url: '/upload',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    revert: {
                        url: '/delete',
                        headers: {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                        }
                    }
                }
            });
            FilePond.create(
                document.querySelector('input[id="images_gallery"]')
            );

            tinymce.init({
                selector: '#short_description',
                setup: function(editor) {
                    editor.on('init', function(e) {
                        editor.setContent('{!! $product->short_description !!}');
                    });
                }
            });

            tinymce.init({
                selector: '#description',
                setup: function(editor) {
                    editor.on('init', function(e) {
                        editor.setContent('{!! $product->description !!}');
                    });
                }
            });

        </script>
    @endsection
</x-app-layout>
