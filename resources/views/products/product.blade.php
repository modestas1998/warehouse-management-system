<x-app-layout>
    <x-slot name="header">
        {{ $product->name }}
        <x-add-new href="{{ route('product.edit', ['id' => $product->id]) }}">
            Edit {{ $product->name }} product
        </x-add-new>
    </x-slot>

    @if (Session::has('success'))
        <div id="alert-additional-content-3" class="p-4 mb-4 bg-green-100 rounded-lg dark:bg-green-200" role="alert">
            <div class="flex items-center">
                <svg class="mr-2 w-5 h-5 text-green-700 dark:text-green-800" fill="currentColor" viewBox="0 0 20 20"
                    xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd"
                        d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z"
                        clip-rule="evenodd"></path>
                </svg>
                <h3 class="text-lg font-medium text-green-700 dark:text-green-800">Success</h3>
            </div>
            <div class="mt-2 mb-4 text-sm text-green-700 dark:text-green-800">
                {{ Session::get('success') }}
            </div>
        </div>
    @endif

    <div class="bg-white overflow-hidden shadow-md sm:rounded-lg">
        <div class="p-6 border-b border-gray-200">
            <div class="grid grid-cols-2 gap-4">
                <div>
                    <x-label for="gallery" :value="__('Product gallery')" />
                    <div style="--swiper-navigation-color: #fff; --swiper-pagination-color: #fff"
                        class="swiper mySwiper2">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <img src="{{ asset('storage/featuredImages/' . auth()->user()->current_team_id . '/' . $product->featured_image) }}"
                                    alt="">
                            </div>
                            @foreach ($product->images()->get() as $image)
                                <div class="swiper-slide">
                                    <img src="{{ asset('storage/galleryImages/' . auth()->user()->current_team_id . '/' . $image->image_name) }}"
                                        alt="">
                                </div>
                            @endforeach
                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>
                    <div thumbsSlider="" class="swiper mySwiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <img src="{{ asset('storage/featuredImages/' . auth()->user()->current_team_id . '/' . $product->featured_image) }}"
                                    alt="">
                            </div>
                            @foreach ($product->images()->get() as $image)
                                <div class="swiper-slide">
                                    <img src="{{ asset('storage/galleryImages/' . auth()->user()->current_team_id . '/' . $image->image_name) }}"
                                        alt="">
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div>
                    <x-label for="short_description" :value="__('Product short description')" />
                    <div class="shadow-md mb-5 rounded p-5">
                        {!! $product->short_description !!}
                    </div>

                    <x-label for="short_description" :value="__('Product description')" />
                    <div class="shadow-md mb-5 rounded p-5">
                        {!! $product->description !!}
                    </div>

                    <div class="grid grid-cols-3 gap-4 mb-5 shadow-md p-5">
                        <div class="border-r">
                            <x-label for="short_description" :value="__('Product price')" />
                            @if ($product->regular_price > $product->sale_price)
                                <p class="line-through mt-2">{{ $product->regular_price }} $</p>
                                <p class="mb-5">{{ $product->sale_price }} $</p>
                            @else
                                <p class="mb-5">{{ $product->regular_price }} $</p>
                            @endif
                        </div>
                        <div class="border-r">
                            <x-label for="quantity" :value="__('Quantity')" />
                            <p class="mb-5 mt-2">{{ $product->quantity }}</p>
                        </div>
                        <div>
                            <x-label for="sku" :value="__('SKU')" />
                            <p class="mb-5 mt-2">{{ $product->sku }}</p>
                        </div>
                    </div>

                    <x-label for="attributes" :value="__('Product attributes')" />
                    <div class="grid grid-cols-2 gap-2 mb-5">
                        @foreach ($product->attributes as $attribute)
                            <div class="bg-blue-200 rounded p-2">{{ $attribute->name }}</div>
                            <div class="bg-blue-100 rounded p-2">
                                @foreach ($attribute->values()->get() as $value)
                                    {{ $value->name }}
                                @endforeach
                            </div>
                        @endforeach
                    </div>

                    <x-label for="categories" :value="__('Product categories')" />
                    <div class="flex flex-wrap">
                        @foreach ($product->categories as $category)
                            <span class="p-2 m-1 bg-amber-100 rounded">{{ $category->name }}</span>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    @section('scripts')
        <script>
            var swiper = new Swiper(".mySwiper", {
                spaceBetween: 10,
                slidesPerView: 5,
                freeMode: true,
                watchSlidesProgress: true,
            });
            var swiper2 = new Swiper(".mySwiper2", {
                spaceBetween: 10,
                navigation: {
                    nextEl: ".swiper-button-next",
                    prevEl: ".swiper-button-prev",
                },
                thumbs: {
                    swiper: swiper,
                },
            });
        </script>
    @endsection

</x-app-layout>
