<x-app-layout>
    <x-slot name="header">
        {{ __('Create a new chapter') }}
    </x-slot>

    <div class="bg-white overflow-hidden shadow-md sm:rounded-lg mb-5">
        <div class="p-6 border-b border-gray-200">
            <form method="post" action="{{ route('teams.store') }}">
                {!! csrf_field() !!}

                    <label class="block text-sm text-gray-700">Name</label>

                    <div class="flex items-center py-2 w-1/2">
                        <input type="text" class="block mt-1 w-full rounded-md form-input focus:border-indigo-600" name="name" value="{{ old('name') }}">

                        <button type="submit" class="flex-shrink-0 bg-indigo-500 hover:bg-indigo-700 text-sm text-white py-2 px-4 ml-5 rounded ease-out duration-300">
                            <i class="fa fa-btn fa-save mr-2"></i>Save
                        </button>
                    </div>
                    @if ($errors->has('name'))
                        <span class="text-sm text-red-500 ml-1">
                            {{ $errors->first('name') }}
                        </span>
                    @endif
                </div>
            </form>
        </div>
    </div>
</x-app-layout>
