<x-app-layout>
    <x-slot name="header">
        {{ __('Members of chapter') }} "{{ $team->name }}"
    </x-slot>

    <div class="bg-white overflow-hidden shadow-md sm:rounded-lg mb-5">
        <div class="p-6 border-b border-gray-200">
            <table class="min-w-full leading-normal">
                <thead>
                    <tr>
                        <th
                            class="px-5 py-3 text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
                            Name</th>
                        <th
                            class="px-5 py-3 text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
                            Action</th>
                    </tr>
                </thead>
                @foreach ($team->users as $user)
                    <tr class="hover:bg-gray-100">
                        <td class="px-5 py-5 text-sm border-b border-gray-200">{{ $user->name }}</td>
                        <td class="px-5 py-5 text-sm border-b border-gray-200">
                            @if (auth()->user()->isOwnerOfTeam($team))
                                @if (auth()->user()->getKey() !== $user->getKey())
                                    <form style="display: inline-block;"
                                        action="{{ route('teams.members.destroy', [$team, $user]) }}" method="post">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="_method" value="DELETE" />
                                        <button class="py-2 px-4 mr-2 text-center bg-rose-600 rounded-md text-white text-sm hover:bg-rose-500 ease-out duration-300"><i class="fa fa-trash-o mr-2"></i>Delete</button>
                                    </form>
                                @endif
                            @endif
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
    <div class="bg-white overflow-hidden shadow-md sm:rounded-lg mb-5">
        <div class="p-6 border-b border-gray-200">
            <table class="min-w-full leading-normal">
                <thead>
                    <tr>
                        <th
                            class="px-5 py-3 text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
                            E-Mail</th>
                        <th
                            class="px-5 py-3 text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
                            Action</th>
                    </tr>
                </thead>
                @foreach ($team->invites as $invite)
                    @if ($team->invites->count() > 0)
                        <tr class="hover:bg-gray-100">
                            <td class="px-5 py-5 text-sm border-b border-gray-200">{{ $invite->email }}</td>
                            <td class="px-5 py-5 text-sm border-b border-gray-200">
                                <a href="{{ route('teams.members.resend_invite', $invite) }}"
                                    class="py-2 px-4 text-center bg-emerald-600 rounded-md text-white text-sm hover:bg-emerald-500 ease-out duration-300">
                                    <i class="fa fa-envelope-o mr-2"></i> Resend invite
                                </a>
                            </td>
                        </tr>
                    @else
                        <div class="flex justify-center align-center py-32">
                            No invites
                        </div>
                    @endif
                @endforeach
            </table>
        </div>
    </div>


    <div class="bg-white overflow-hidden shadow-md sm:rounded-lg">
        <div class="p-6 border-b border-gray-200">
            <h3 class="py-5">Invite to chapter "{{ $team->name }}"</h3>
            <form method="post" action="{{ route('teams.members.invite', $team) }}">
                {!! csrf_field() !!}
                <label class="block text-sm text-gray-700">E-Mail Address</label>
                <div class="flex items-center py-2 w-1/2">
                    <input type="email" class="block mt-1 w-full rounded-md form-input focus:border-indigo-600"
                        name="email" value="{{ old('email') }}">
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                    <button type="submit"
                        class="flex-shrink-0 bg-indigo-500 hover:bg-indigo-700 text-sm text-white py-2 px-4 ml-5 rounded ease-out duration-300">
                        <i class="fa fa-btn fa-envelope-o mr-2"></i>Invite to chapter
                    </button>
                </div>
            </form>
        </div>
    </div>

</x-app-layout>
