<x-app-layout>
    <x-slot name="header">
        {{ __('My chapters') }}
        <x-add-new href="{{ route('teams.create') }}">
            {{ __('Create new chapter') }}
        </x-add-new>
    </x-slot>

    <div class="bg-white overflow-hidden shadow-md sm:rounded-lg">
        <div class="p-6 border-b border-gray-200">
            <table class="min-w-full leading-normal">
                <thead>
                    <tr>
                        <th
                            class="px-5 py-3 text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
                            Name</th>
                        <th
                            class="px-5 py-3 text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
                            Status</th>
                        <th
                            class="px-5 py-3 text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($teams as $team)
                        <tr class="hover:bg-gray-100">
                            <td class="px-5 py-5 text-sm border-b border-gray-200">{{ $team->name }}</td>
                            <td class="px-5 py-5 text-sm border-b border-gray-200">
                                @if (auth()->user()->isOwnerOfTeam($team))
                                    <span class="label label-success">Owner</span>
                                @else
                                    <span class="label label-primary">Member</span>
                                @endif
                            </td>
                            <td class="px-5 py-5 text-sm border-b border-gray-200 text-right">
                                @if (is_null(auth()->user()->currentTeam) ||
    auth()->user()->currentTeam->getKey() !== $team->getKey())
                                    <a href="{{ route('teams.switch', $team) }}"
                                        class="py-2 px-4 mr-2 text-center bg-emerald-600 rounded-md text-white text-sm hover:bg-emerald-500 ease-out duration-300">
                                        <i class="fa fa-sign-in"></i> Switch
                                    </a>
                                @else
                                    <span class="px-4 py-2 mr-2 rounded border-2 border-indigo-600">Current chapter</span>
                                @endif

                                <a href="{{ route('teams.members.show', $team) }}"
                                    class="py-2 px-4 mr-2 text-center bg-emerald-600 rounded-md text-white text-sm hover:bg-emerald-500 ease-out duration-300">
                                    <i class="fa fa-users"></i> Members
                                </a>

                                @if (auth()->user()->isOwnerOfTeam($team))
                                    <a href="{{ route('teams.edit', $team) }}"
                                        class="py-2 px-4 mr-2 text-center bg-indigo-600 rounded-md text-white text-sm hover:bg-indigo-500 ease-out duration-300">
                                        <i class="fa fa-pencil"></i>
                                    </a>

                                    <form style="display: inline-block;" action="{{ route('teams.destroy', $team) }}"
                                        method="post">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="_method" value="DELETE" />
                                        <button
                                            class="py-2 px-4 mr-2 text-center bg-rose-600 rounded-md text-white text-sm hover:bg-rose-500 ease-out duration-300"><i
                                                class="fa fa-trash-o"></i>
                                        </button>
                                    </form>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

</x-app-layout>
