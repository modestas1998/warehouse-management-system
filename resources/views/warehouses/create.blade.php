<x-app-layout>
    <x-slot name="header">
        {{ __('Create new warehouses') }}
    </x-slot>

    <x-errors class="mb-4" :errors="$errors" />

    <div class="bg-white overflow-hidden shadow-md sm:rounded-lg">
        <div class="p-6 border-b border-gray-200">
            @livewire('warehouse-create')
        </div>
    </div>
</x-app-layout>
