<x-app-layout>
    <x-slot name="header">
        {{ __('Warehouses') }}
        <x-add-new href="{{ route('warehouse.create') }}">
            Add new warehouse
        </x-add-new>
    </x-slot>

    @if ($warehouses->count() != 0)
        <div class="bg-white overflow-hidden shadow-md sm:rounded-lg">
            <div class="p-6 border-b border-gray-200">
                @livewire('warehouses-table')
            </div>
        </div>
    @else
        <div class="flex justify-center align-center py-32">
            No warehouses
        </div>
    @endif

</x-app-layout>
