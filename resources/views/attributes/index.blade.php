<x-app-layout>
    <x-slot name="header">
        {{ __('Attributes') }}
    </x-slot>

    <div class="overflow-hidden">
        @livewire('attributes-table')
    </div>

</x-app-layout>
