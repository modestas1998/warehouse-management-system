@props(['disabled' => false])

<textarea {!! $attributes->merge(['class' => 'form-control block w-full px-3 py-1.5 text-base font-normal bg-white bg-clip-padding border border-solid rounded transition ease-in-out m-0 focus:border-indigo-600']) !!}>
</textarea>
