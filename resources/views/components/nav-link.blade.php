@props(['active'])

@php
    $classes = ($active ?? false)
                ? 'flex items-center py-5 px-6 bg-gray-400 bg-opacity-25 text-gray-100'
                : 'flex items-center py-5 px-6 text-gray-100 duration-300 hover:bg-gray-400 hover:bg-opacity-25';
@endphp

<a {{ $attributes->merge(['class' => $classes]) }}>
    {{ $icon ?? '' }}
    <span class="mx-3">{{ $slot }}</span>
</a>
