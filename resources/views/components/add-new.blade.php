@php
    $classes = 'py-2 px-4 text-center bg-neutral-600 rounded-md text-white text-sm hover:bg-neutral-500 ease-out duration-300'
@endphp

<a {{ $attributes->merge(['class' => $classes]) }}>
    {{ $icon ?? '' }}
    <span class="mx-3">{{ $slot }}</span>
</a>
