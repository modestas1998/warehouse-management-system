<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Team;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(StatusesSeeder::class);

        $user = User::create([
            'name' => 'John Doe',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('Admin123.'),
            'role_id' => 1,
        ]);

        $team = Team::create([
            'owner_id' => $user->id,
            'name' => $user->name . "'s Team",
        ]);

        $user->attachTeam($team);

        $this->call(TeamSeeder::class);

        $users = User::all();

        foreach($users as $user) {
            $user->teams()->attach(rand(1, 20));
        }

        $this->call(CategorySeeder::class);
    }
}
