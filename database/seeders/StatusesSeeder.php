<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = [
            ['id' => 1, 'name' => 'On hold'],
            ['id' => 2, 'name' => 'Unpaid'],
            ['id' => 3, 'name' => 'Payment completed'],
            ['id' => 4, 'name' => 'Shipped'],
            ['id' => 5, 'name' => 'Completed'],
            ['id' => 6, 'name' => 'Canceled'],
        ];

        DB::table('statuses')->insert($statuses);
    }
}
