<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
            'short_description' => $this->faker->text(200),
            'description' => $this->faker->text(400),
            'regular_price' => $this->faker->randomFloat(null, 0, 1000),
            'sale_price' => $this->faker->randomFloat(null, 0, 1000),
            'sku' => $this->faker->ean13,
            'quantity' => $this->faker->randomDigit,
            'featured_image' => null,
            'warehouse_id' => rand(0, 19),
        ];
    }
}
