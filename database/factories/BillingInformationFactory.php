<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\BillingInformation>
 */
class BillingInformationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'order_id' => rand(1, 10),
            'name' => $this->faker->firstName,
            'surname' => $this->faker->lastName,
            'company_name' => $this->faker->company,
            'company_address' => $this->faker->address,
            'company_code' => $this->faker->numberBetween(10000000, 99999999),
            'country' => $this->faker->country,
            'city' => $this->faker->city,
            'address' => $this->faker->address,
            'zip' => $this->faker->postcode,
        ];
    }
}
