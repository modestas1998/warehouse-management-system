<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Order>
 */
class OrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'status_id' => rand(1, 6),
            'pdf_invoice' => $this->faker->name,
            'team_id' => rand(0, 19),
            'number' => rand(1, 100)
        ];
    }
}
