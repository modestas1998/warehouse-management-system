<?php

use App\Http\Controllers\UploadController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

Route::middleware('auth')->group(function () {

    Route::get('dashboard', [\App\Http\Controllers\DashboardController::class, 'index'])->name('dashboard');

    Route::get('profile', [\App\Http\Controllers\ProfileController::class, 'show'])->name('profile.show');
    Route::put('profile', [\App\Http\Controllers\ProfileController::class, 'update'])->name('profile.update');

    Route::post('upload', [\App\Http\Controllers\UploadController::class, 'store']);
    Route::delete('upload/delete', [\App\Http\Controllers\UploadController::class, 'delete']);

    //admin routes
    Route::group(['middleware' => 'role:admin', 'prefix' => 'admin'], function() {
        Route::get('teams', [App\Http\Controllers\Teamwork\TeamController::class, 'index'])->name('teams.index');
        Route::get('create', [App\Http\Controllers\Teamwork\TeamController::class, 'create'])->name('teams.create');
        Route::post('teams', [App\Http\Controllers\Teamwork\TeamController::class, 'store'])->name('teams.store');
        Route::get('edit/{id}', [App\Http\Controllers\Teamwork\TeamController::class, 'edit'])->name('teams.edit');
        Route::put('edit/{id}', [App\Http\Controllers\Teamwork\TeamController::class, 'update'])->name('teams.update');
        Route::delete('destroy/{id}', [App\Http\Controllers\Teamwork\TeamController::class, 'destroy'])->name('teams.destroy');
        Route::get('switch/{id}', [App\Http\Controllers\Teamwork\TeamController::class, 'switchTeam'])->name('teams.switch');

        Route::get('members/{id}', [App\Http\Controllers\Teamwork\TeamMemberController::class, 'show'])->name('teams.members.show');
        Route::get('members/resend/{invite_id}', [App\Http\Controllers\Teamwork\TeamMemberController::class, 'resendInvite'])->name('teams.members.resend_invite');
        Route::post('members/{id}', [App\Http\Controllers\Teamwork\TeamMemberController::class, 'invite'])->name('teams.members.invite');
        Route::delete('members/{id}/{user_id}', [App\Http\Controllers\Teamwork\TeamMemberController::class, 'destroy'])->name('teams.members.destroy');

        Route::get('warehouses', [\App\Http\Controllers\WarehouseController::class, 'index'])->name('warehouse.index');
        Route::get('warehouses/create', [\App\Http\Controllers\WarehouseController::class, 'create'])->name('warehouse.create');

        Route::get('products', [\App\Http\Controllers\ProductController::class, 'index'])->name('product.index');
        Route::get('products/{id}-create', [\App\Http\Controllers\ProductController::class, 'create'])->name('product.create');
        Route::post('products/{id}-create/store', [\App\Http\Controllers\ProductController::class, 'store'])->name('product.store');
        Route::get('products/{id}', [\App\Http\Controllers\ProductController::class, 'show'])->name('product.show');
        Route::get('products/{id}/edit', [\App\Http\Controllers\ProductController::class, 'edit'])->name('product.edit');
        Route::put('products/{id}/edit/update', [\App\Http\Controllers\ProductController::class, 'update'])->name('product.update');

        Route::get('categories', [\App\Http\Controllers\CategoryController::class, 'index'])->name('category.index');

        Route::get('attributes', [\App\Http\Controllers\AttributeController::class, 'index'])->name('attribute.index');

        Route::get('orders', [\App\Http\Controllers\OrderController::class, 'index'])->name('order.index');
        Route::get('orders/create', [\App\Http\Controllers\OrderController::class, 'create'])->name('order.create');
    });

    Route::group(['middleware' => 'role:products-manager', 'prefix' => 'products-manager'], function() {
        //
    });

    Route::group(['middleware' => 'role:accountant', 'prefix' => 'accountant'], function() {
        //
    });

    Route::group(['middleware' => 'role:user', 'prefix' => 'user'], function() {
        //
    });

});
