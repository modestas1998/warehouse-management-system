<?php

namespace Tests\Feature;

use App\Http\Requests\ProfileUpdateRequest;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class ProfileUpdateRequestTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_profile_update_validation_rules()
    {
        $request = new ProfileUpdateRequest();

        $this->assertEquals([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'string', 'max:255', Rule::unique('users')->ignore(Auth::user())],
            'password' => ['nullable', 'string', 'confirmed', 'min:8'],
        ], $request->rules());
    }

    public function test_profile_update_verifies_authorized()
    {
        $user = User::factory()->create();
        $this->actingAs($user);
        $formRequest = new ProfileUpdateRequest();
        $this->assertTrue($formRequest->authorize());
    }
}
