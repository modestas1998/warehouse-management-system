<?php

namespace Tests\Feature;

use App\Models\Order;
use App\Models\ShippingInformation;
use App\Models\Team;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ShippingInformationTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_shipping_information_belongs_to_order()
    {
        $team = Team::factory()->create();
        $order = Order::factory()->create(['team_id' => $team->id]);
        $shippingInformation = ShippingInformation::factory()->create(['order_id' => $order->id]);

        $this->assertInstanceOf(Order::class, $shippingInformation->order);
    }
}
