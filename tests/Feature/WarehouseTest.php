<?php

namespace Tests\Feature;

use App\Http\Livewire\WarehouseCreate;
use App\Http\Livewire\WarehousesTable;
use App\Models\Team;
use App\Models\User;
use App\Models\Warehouse;
use Database\Seeders\RoleSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;

class WarehouseTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_warehouse_screen_can_be_rendered()
    {
        $this->seed(RoleSeeder::class);
        $user = User::factory()->create();
        $this->actingAs($user);

        $response = $this->get('/admin/warehouses');

        if($user->role->id === 1) {
            $response->assertStatus(200);
        } elseif ($user->role->id === 2) {
            $response->assertStatus(403);
        } elseif ($user->role->id === 3) {
            $response->assertStatus(403);
        } elseif ($user->role->id === 4) {
            $response->assertStatus(403);
        }
    }

    public function test_warehouse_create_screen_can_be_rendered()
    {
        $this->seed(RoleSeeder::class);
        $user = User::factory()->create();
        $this->actingAs($user);

        $response = $this->get('/admin/warehouses/create');

        if($user->role->id === 1) {
            $response->assertStatus(200);
        } elseif ($user->role->id === 2) {
            $response->assertStatus(403);
        } elseif ($user->role->id === 3) {
            $response->assertStatus(403);
        } elseif ($user->role->id === 4) {
            $response->assertStatus(403);
        }
    }

    public function test_user_can_create_warehouse()
    {
        $name = $this->faker->name;
        $user = User::factory()->create();
        $this->actingAs($user);

        $team = Team::factory()->create();
        $user->attachTeam($team);

        Livewire::test(WarehouseCreate::class)
            ->set('name', $name)
            ->set('address', $this->faker->address)
            ->set('country', $this->faker->country)
            ->set('city', $this->faker->city)
            ->call('storePost');

        $this->assertTrue(Warehouse::whereName($name)->exists());
    }

    public function test_delete_confirm_message()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $team = Team::factory()->create();
        $warehouse = Warehouse::factory()->create(['team_id' => $team->id]);
        $warehouses = Warehouse::all();

        $modalData = [
            'type' => 'warning',
            'title' => 'Are you sure you want to delete this warehouse with all products?',
            'text' => '',
            'id' => $warehouse->id,
        ];

        Livewire::test(WarehousesTable::class)
            ->set('warehouses', $warehouses)
            ->call('deleteConfirm', 0)
            ->assertDispatchedBrowserEvent('swal:confirm', $modalData);
    }

    public function test_user_can_delete_warehouse()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $team = Team::factory()->create();
        $warehouse = Warehouse::factory()->create(['team_id' => $team->id]);

        Livewire::test(WarehousesTable::class)
            ->call('delete', $warehouse->id);

        $this->assertTrue(Warehouse::whereId($warehouse->id)->doesntExist());
    }

    public function test_user_can_edit_warehouse()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $team = Team::factory()->create();
        $warehouse = Warehouse::factory()->create(['team_id' => $team->id]);
        $warehouses = Warehouse::all();

        $warehouse->name = 'name';
        $warehouse->save();

        Livewire::test(WarehousesTable::class)
            ->set('warehouses', $warehouses)
            ->call('saveWarehouse', 0);

        $this->assertEquals('name', $warehouse->name);
    }

    public function test_edit_warehouse_set()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $index = rand(0, 3);

        Livewire::test(WarehousesTable::class)
            ->set('editedWarehouseIndex', $index)
            ->call('editWarehouse', $index)
            ->assertSet('editedWarehouseIndex', $index);
    }

    public function test_edit_field_set()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $index = rand(0, 3);
        $fieldName = ['name', 'country', 'city', 'address'];

        Livewire::test(WarehousesTable::class)
            ->set('editedWarehouseField', $index . '.' . $fieldName[$index])
            ->call('editWarehouseField', $index, $fieldName[$index])
            ->assertSet('editedWarehouseField', $index . '.' . $fieldName[$index]);
    }
}
