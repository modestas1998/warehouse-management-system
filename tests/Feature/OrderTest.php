<?php

namespace Tests\Feature;

use App\Models\BillingInformation;
use App\Models\Order;
use App\Models\Product;
use App\Models\ShippingInformation;
use App\Models\Team;
use App\Models\User;
use App\Models\Warehouse;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class OrderTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_order_has_one_billing_information()
    {
        $team = Team::factory()->create();
        $order = Order::factory()->create(['team_id' => $team->id]);
        BillingInformation::factory()->create(['order_id' => $order->id]);

        $this->assertInstanceOf(BillingInformation::class, $order->billingInformation);
    }

    public function test_order_has_one_shipping_information()
    {
        $team = Team::factory()->create();
        $order = Order::factory()->create(['team_id' => $team->id]);
        ShippingInformation::factory()->create(['order_id' => $order->id]);

        $this->assertInstanceOf(ShippingInformation::class, $order->shippingInformation);
    }

    // public function test_order_has_many_products()
    // {
    //     $team = Team::factory()->create();
    //     $warehouse = Warehouse::factory()->create(['team_id' => $team->id]);
    //     $product = Product::factory()->create(['warehouse_id' => $warehouse->id]);
    //     $order = Order::factory()->create(['team_id' => $team->id]);

    //     $this->assertTrue('Illuminate\Database\Eloquent\Collection', $order->products);
    // }
}
