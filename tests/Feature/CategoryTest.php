<?php

namespace Tests\Feature;

use App\Http\Livewire\CategoriesTable;
use App\Http\Livewire\CategoryEdit;
use App\Models\Category;
use App\Models\Team;
use App\Models\User;
use Database\Seeders\RoleSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;

class CategoryTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_categories_screen_can_be_rendered()
    {
        $this->seed(RoleSeeder::class);
        $user = User::factory()->create();
        $this->actingAs($user);

        $response = $this->get(route('category.index'));

        if($user->role->id === 1) {
            $response->assertStatus(200);
        } else {
            $response->assertStatus(403);
        }
    }

    public function test_user_can_create_category()
    {
        $name = $this->faker->name;
        $user = User::factory()->create();
        $this->actingAs($user);

        $team = Team::factory()->create();
        $user->attachTeam($team);

        Livewire::test(CategoriesTable::class)
            ->set('name', $name)
            ->set('selectedCategoryId', 0)
            ->call('storeCategory');

        $this->assertTrue(Category::whereName($name)->exists());
    }

    public function test_delete_confirm_message()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $team = Team::factory()->create();
        $category = Category::factory()->create(['parent_id' => 0, 'team_id' => $team->id]);
        $modalData = [
            'type' => 'warning',
            'title' => 'Are you sure you want to delete this category with all products?',
            'text' => '',
            'id' => $category->id,
        ];

        Livewire::test(CategoriesTable::class)
            ->call('deleteConfirm', $category->id)
            ->assertDispatchedBrowserEvent('swal:confirm', $modalData);
    }

    public function test_user_can_delete_category()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $team = Team::factory()->create();
        $category = Category::factory()->create(['parent_id' => 0, 'team_id' => $team->id]);

        Livewire::test(CategoriesTable::class)
            ->call('delete', $category->id);

        $this->assertTrue(Category::whereId($category->id)->doesntExist());
    }

    public function test_user_can_edit_category()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $team = Team::factory()->create();
        $category = Category::factory()->create(['parent_id' => 0, 'team_id' => $team->id]);
        $categories = Category::where('team_id', $user->current_team_id)
            ->get();
        $modalData = [
            'type' => 'success',
            'title' => 'Category updated successfully!',
            'text' => ''
        ];

        Livewire::test(CategoryEdit::class, ['category' => $category, 'categories' => $categories])
            ->set('categoryParentId', 0)
            ->set('categoryName', $category->name)
            ->call('saveCategory')
            ->assertDispatchedBrowserEvent('swal:modal', $modalData);;
    }

    public function test_category_has_many_childrens()
    {
        $team = Team::factory()->create();
        $category = Category::factory()->create(['team_id' => $team->id]);
        $children = Category::factory()->create(['parent_id' => $category->id, 'team_id' => $team->id]);

        $this->assertTrue($category->categoryChildrens->contains($children));
    }

    public function test_category_belongs_to_parent()
    {
        $team = Team::factory()->create();
        $category = Category::factory()->create(['team_id' => $team->id]);
        $children = Category::factory()->create(['parent_id' => $category->id, 'team_id' => $team->id]);

        $this->assertInstanceOf(Category::class, $children->parent);
    }
}
