<?php

namespace Tests\Feature;

use App\Http\Livewire\ProductsTable;
use App\Http\Requests\ProductCreateRequest;
use App\Http\Requests\ProductEditRequest;
use App\Models\Category;
use App\Models\Product;
use App\Models\Team;
use App\Models\User;
use App\Models\Warehouse;
use App\Models\Attribute;
use App\Models\Order;
use App\Models\ProductImage;
use Database\Seeders\RoleSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;

class ProductTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_product_create_validation_rules()
    {
        $request = new ProductCreateRequest();

        $this->assertEquals([
            'name' => ['required', 'max:100'],
            'short_description' => ['required'],
            'description' => ['required'],
            'regular_price' => ['required', 'numeric'],
            'sale_price' => ['required', 'numeric'],
            'sku' => ['required'],
            'quantity' => ['required', 'numeric'],
        ], $request->rules());
    }

    public function test_product_edit_validation_rules()
    {
        $request = new ProductEditRequest();

        $this->assertEquals([
            'name' => ['required', 'max:100'],
            'short_description' => ['required'],
            'description' => ['required'],
            'regular_price' => ['required', 'numeric'],
            'sale_price' => ['required', 'numeric'],
            'sku' => ['required'],
            'quantity' => ['required', 'numeric'],
        ], $request->rules());
    }

    public function test_create_request_verifies_authorized()
    {
        $user = User::factory()->create();
        $this->actingAs($user);
        $formRequest = new ProductCreateRequest();
        $this->assertTrue($formRequest->authorize());
    }

    public function test_edit_request_verifies_authorized()
    {
        $user = User::factory()->create();
        $this->actingAs($user);
        $formRequest = new ProductEditRequest();
        $this->assertTrue($formRequest->authorize());
    }

    public function test_products_screen_can_be_rendered()
    {
        $this->seed(RoleSeeder::class);
        $user = User::factory()->create();
        $this->actingAs($user);

        $response = $this->get('/admin/products');

        if($user->role->id === 1) {
            $response->assertStatus(200);
        } else {
            $response->assertStatus(403);
        }
    }

    public function test_create_product_view_can_be_rendered()
    {
        $this->seed(RoleSeeder::class);
        $user = User::factory()->create();
        $this->actingAs($user);

        $team = Team::factory()->create();
        $user->attachTeam($team);

        $warehouse = Warehouse::factory()->create(['team_id' => $team->id]);

        $response = $this->get('/admin/products/'. $warehouse->id .'-create');

        if($user->role->id === 1) {
            $response->assertStatus(200);
        } else {
            $response->assertStatus(403);
        }
    }

    public function test_product_belongs_to_many_categories()
    {
        $team = Team::factory()->create();
        $warehouse = Warehouse::factory()->create(['team_id' => $team->id]);
        $product = Product::factory()->create(['warehouse_id' => $warehouse->id]);
        $category = Category::factory()->create(['team_id' => $team->id]);

        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $product->categories);
    }

    public function test_product_belongs_to_many_attributes()
    {
        $team = Team::factory()->create();
        $warehouse = Warehouse::factory()->create(['team_id' => $team->id]);
        $product = Product::factory()->create(['warehouse_id' => $warehouse->id]);
        $attribute = Attribute::factory()->create(['team_id' => $team->id]);

        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $product->attributes);
    }

    // public function test_product_belongs_to_many_orders()
    // {
    //     $team = Team::factory()->create();
    //     $warehouse = Warehouse::factory()->create(['team_id' => $team->id]);
    //     $product = Product::factory()->create(['warehouse_id' => $warehouse->id]);
    //     $order = Order::factory()->create(['team_id' => $team->id]);

    //     $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $product->orders);
    // }

    public function test_product_has_many_images()
    {
        $team = Team::factory()->create();
        $warehouse = Warehouse::factory()->create(['team_id' => $team->id]);
        $product = Product::factory()->create(['warehouse_id' => $warehouse->id]);
        $productImage = ProductImage::factory()->create(['product_id' => $product->id]);

        $this->assertTrue($product->images->contains($productImage));
    }

    public function test_delete_confirm_message()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $team = Team::factory()->create();
        $warehouse = Warehouse::factory()->create(['team_id' => $team->id]);
        $product = Product::factory()->create(['warehouse_id' => $warehouse->id]);

        $modalData = [
            'type' => 'warning',
            'title' => 'Are you sure you want to delete this category with all products?',
            'text' => '',
            'id' => $product->id,
        ];

        Livewire::test(ProductsTable::class)
            ->call('deleteConfirm', $product->id)
            ->assertDispatchedBrowserEvent('swal:confirm', $modalData);
    }

    public function test_user_can_delete_product()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $team = Team::factory()->create();
        $warehouse = Warehouse::factory()->create(['team_id' => $team->id]);
        $product = Product::factory()->create(['warehouse_id' => $warehouse->id]);

        Livewire::test(ProductsTable::class)
            ->call('delete', $product->id);

        $this->assertTrue(Product::whereId($product->id)->doesntExist());
    }
}
