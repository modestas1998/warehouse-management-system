<?php

namespace Tests\Feature;

use App\Http\Livewire\AttributeValues;
use App\Models\Team;
use App\Models\User;
use App\Models\Attribute;
use App\Models\AttributeValue;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;

class AttributeValuesTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_edit_attribute_set()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $team = Team::factory()->create();
        $attribute = Attribute::factory()->create(['team_id' => $team->id]);
        $index = 0;

        Livewire::test(AttributeValues::class, ['attribute' => $attribute])
            ->set('editedValueIndex', $index)
            ->call('editValue', $index)
            ->assertSet('editedValueIndex', $index);
    }

    public function test_edit_attribute_field_set()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $team = Team::factory()->create();
        $attribute = Attribute::factory()->create(['team_id' => $team->id]);
        $index = 0;
        $fieldName = ['name'];

        Livewire::test(AttributeValues::class, ['attribute' => $attribute])
            ->set('editedValueField', $index . '.' . $fieldName[$index])
            ->call('editValueField', $index, $fieldName[$index])
            ->assertSet('editedValueField', $index . '.' . $fieldName[$index]);
    }

    public function test_attribute_value_belongs_to_attribute()
    {
        $team = Team::factory()->create();
        $attribute = Attribute::factory()->create(['team_id' => $team->id]);
        $value = AttributeValue::factory()->create(['attribute_id' => $attribute->id]);

        $this->assertInstanceOf(Attribute::class, $value->attribute);
    }
}
