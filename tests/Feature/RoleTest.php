<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\Role;
use App\Models\Team;
use App\Models\Warehouse;
use App\Services\RoleService;
use Database\Seeders\RoleSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Support\Str;

class RoleTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_role_middleware()
    {
        $this->seed(RoleSeeder::class);
        $user1 = User::factory()->create(['role_id' => 1]);
        $user2 = User::factory()->create(['role_id' => 2]);
        $user3 = User::factory()->create(['role_id' => 3]);
        $user4 = User::factory()->create(['role_id' => 4]);

        if(Str::slug($user1->role->name, '-') == 'admin') {
            try
            {
                (new RoleService)->checkRole(Str::slug($user1->role->name, '-'), 2);
            }
            catch (\Exception $e)
            {
                $response = $e->getStatusCode();
            }

            $this->assertEquals(403, $response);
        }

        if (Str::slug($user2->role->name, '-') == 'user') {
            try
            {
                (new RoleService)->checkRole(Str::slug($user2->role->name, '-'), 1);
            }
            catch (\Exception $e)
            {
                $response = $e->getStatusCode();
            }

            $this->assertEquals(403, $response);
        }

        if (Str::slug($user3->role->name, '-') == 'products-manager') {
            try
            {
                (new RoleService)->checkRole(Str::slug($user3->role->name, '-'), 1);
            }
            catch (\Exception $e)
            {
                $response = $e->getStatusCode();
            }

            $this->assertEquals(403, $response);
        }

        if (Str::slug($user4->role->name, '-') == 'accountant') {
            try
            {
                (new RoleService)->checkRole(Str::slug($user4->role->name, '-'), 1);
            }
            catch (\Exception $e)
            {
                $response = $e->getStatusCode();
            }

            $this->assertEquals(403, $response);
        }
    }

    public function test_user_belongs_to_role()
    {
        $this->seed(RoleSeeder::class);
        $user = User::factory()->create();

        $this->assertInstanceOf(Role::class, $user->role);
    }

    public function test_role_has_many_users()
    {
        $this->seed(RoleSeeder::class);
        $user = User::factory()->create();
        $role = $user->role;

        $this->assertTrue($role->users->contains($user));
    }
}
