<?php

namespace Tests\Feature;

use App\Http\Livewire\AttributesTable;
use App\Models\Team;
use App\Models\User;
use App\Models\Attribute;
use App\Models\AttributeValue;
use Database\Seeders\RoleSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;

class AttributeTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_attribute_screen_can_be_rendered()
    {
        $this->seed(RoleSeeder::class);
        $user = User::factory()->create();
        $this->actingAs($user);

        $response = $this->get(route('attribute.index'));

        if($user->role->id === 1) {
            $response->assertStatus(200);
        } else {
            $response->assertStatus(403);
        }
    }

    // public function test_user_can_create_attribute()
    // {
    //     $name = $this->faker->name;
    //     $user = User::factory()->create();
    //     $this->actingAs($user);

    //     $team = Team::factory()->create();
    //     $user->attachTeam($team);

    //     Livewire::test(AttributesTable::class)
    //         ->set('name', $name)
    //         ->call('storeAttribute');

    //     $this->assertTrue(Attribute::whereName($name)->exists());
    // }

    // public function test_delete_confirm_message()
    // {
    //     $user = User::factory()->create();
    //     $this->actingAs($user);

    //     $team = Team::factory()->create();
    //     $attribute = Attribute::factory()->create(['team_id' => $team->id]);
    //     $modalData = [
    //         'type' => 'warning',
    //         'title' => 'Are you sure you want to delete this attribute with all products?',
    //         'text' => '',
    //         'id' => $attribute->id,
    //     ];

    //     Livewire::test(AttributesTable::class)
    //         ->call('deleteConfirm', $attribute->id)
    //         ->assertDispatchedBrowserEvent('swal:confirm', $modalData);
    // }

    // public function test_user_can_delete_category()
    // {
    //     $user = User::factory()->create();
    //     $this->actingAs($user);

    //     $team = Team::factory()->create();
    //     $attribute = Attribute::factory()->create(['team_id' => $team->id]);

    //     Livewire::test(AttributesTable::class)
    //         ->call('delete', $attribute->id);

    //     $this->assertTrue(Attribute::whereId($attribute->id)->doesntExist());
    // }

    // public function test_edit_attribute_set()
    // {
    //     $user = User::factory()->create();
    //     $this->actingAs($user);

    //     $index = 0;

    //     Livewire::test(AttributesTable::class)
    //         ->set('editedAttributeIndex', $index)
    //         ->call('editAttribute', $index)
    //         ->assertSet('editedAttributeIndex', $index);
    // }

    // public function test_edit_attribute_field_set()
    // {
    //     $user = User::factory()->create();
    //     $this->actingAs($user);

    //     $index = 0;
    //     $fieldName = ['name'];

    //     Livewire::test(AttributesTable::class)
    //         ->set('editedAttributeField', $index . '.' . $fieldName[$index])
    //         ->call('editAttributeField', $index, $fieldName[$index])
    //         ->assertSet('editedAttributeField', $index . '.' . $fieldName[$index]);
    // }

    // public function test_user_can_edit_attribute()
    // {
    //     $user = User::factory()->create();
    //     $this->actingAs($user);

    //     $team = Team::factory()->create();
    //     $attribute = Attribute::factory()->create(['team_id' => $team->id]);
    //     $attributes = Attribute::all();

    //     $attribute->name = 'name';
    //     $attribute->save();

    //     Livewire::test(AttributesTable::class)
    //         ->set('attributes', $attributes)
    //         ->call('saveAttribute', 0);

    //     $this->assertEquals('name', $attribute->name);
    // }

    public function test_attribute_has_many_values()
    {
        $user = User::factory()->create();
        $team = Team::factory()->create();
        $attribute = Attribute::factory()->create(['team_id' => $team->id]);
        $value = AttributeValue::factory()->create(['attribute_id' => $attribute->id]);

        $this->assertTrue($attribute->values->contains($value));
    }
}
