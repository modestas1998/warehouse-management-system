<?php

namespace Tests\Feature;

use App\Models\BillingInformation;
use App\Models\Order;
use App\Models\Team;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BillingInformationTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_billing_information_belongs_to_order()
    {
        $team = Team::factory()->create();
        $order = Order::factory()->create(['team_id' => $team->id]);
        $billingInformation = BillingInformation::factory()->create(['order_id' => $order->id]);

        $this->assertInstanceOf(Order::class, $billingInformation->order);
    }
}
